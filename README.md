# Programming Language #

A LL(1) parser for a functional language. Written for a school project at Tesla STEM High School.

**To do**

 :white_large_square: Lists, arrays, maps  
 :white_large_square: Operators: +=, -=, *=, /=, ++, --  
 :white_large_square: Lambdas  
 :white_large_square: More standard lib: string funcs  


### Developers ###

Kyran Adams: s-kadams(at)lwsd(dot)org
