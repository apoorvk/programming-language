package com.proglang.interpreter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.lexer.Lexer;
import com.proglang.parser.Parser;

public class Interpreter {
	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {
			boolean invalid;
			do {
				invalid = false;
				System.out.println("REPL or file input? ");
				String mode = input.nextLine();
				switch (mode.toLowerCase()) {
				case "repl":
					repl(input);
					break;
				case "file":
					readFile(input);
					break;
				default:
					System.err.println("Unknown mode " + mode
							+ "; use 'repl' or 'file'");
					invalid = true;
				}
			} while (invalid);
		}
	}
	
	public static void repl(Scanner input) {
		System.out.println("--- TYPE YOUR COMMAND ---");
		Context c = new Context(input);
		String str;
		while (!((str = input.nextLine()).equals("exit"))) {
			try {
				List<ASTExpr> parsedList = new Parser(new Lexer(str)).parseProgram().getExpressions();
				for (ASTExpr val : parsedList) {
					System.out.println(val.simplify(c).get());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void readFile(Scanner input) {
		Context c = new Context(input);
		System.out.println("Enter file name:");
		String filename = input.nextLine();
		String str;
		try {
			str = new String(Files.readAllBytes(Paths.get(filename)));
			List<ASTExpr> parsedList = new Parser(new Lexer(str)).parseProgram().getExpressions();
			for (ASTExpr val : parsedList) {
				// Don't print; just simplify
				val.simplify(c).get();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}