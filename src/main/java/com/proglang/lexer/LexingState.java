package com.proglang.lexer;

/**
 * Holds the state of the lexer as it is tokenizing.
 * 
 * @author Kyran Adams
 *
 */
public class LexingState {
	// the lexer's current line number
	public int currentLine = 1;
	// the index of the last newline that the lexer tokenized
	public int indexLastNewline = 0;
	// the total index, in characters, the lexer is at from the beginning of
	// the file
	public int index = 0;

	public LexingState(){
		this(1,0,0);
	}
	
	private LexingState(int currentLine, int indexLastNewline, int index) {
		this.currentLine = currentLine;
		this.indexLastNewline = indexLastNewline;
		this.index = index;
	}
	@Override
	public String toString() {
		return " (Line: " + currentLine + ", Position: " + (index - indexLastNewline) + ")";
	}
	public LexingState copy(){
		return new LexingState(currentLine, indexLastNewline, index);
	}

	/**
	 * Updates the lexer's state after it has lexed a new token.
	 * 
	 * @param text
	 */
	public void update(String text) {
		// if the text contains a new line
		if (text.indexOf('\n') != -1) {
			// add number of \n in text to the currentLine
			currentLine += text.length() - text.replace("\n", "").length();
			indexLastNewline = index + text.lastIndexOf('\n');
		}
		index += text.length();
	}
}