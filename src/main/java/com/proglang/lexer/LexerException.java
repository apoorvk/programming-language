package com.proglang.lexer;

@SuppressWarnings("serial")
public class LexerException extends RuntimeException {

	public LexerException(String msg) {
		super(msg);
	}
}
