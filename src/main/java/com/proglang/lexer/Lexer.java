package com.proglang.lexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Lexer {
	// tokens with whitespaces and comments
	private List<Token> fullTokens;
	// tokens without whitespace and comments
	private List<Token> cleanedTokens;
	private int currentToken;

	public Lexer(String s) {
		this.fullTokens = tokenize(s);
		cleanedTokens = getFullTokens().stream().filter((a) -> a.type != TokenType.BLOCK_COMMENT
				&& a.type != TokenType.WHITE_SPACE && a.type != TokenType.COMMENT).collect(Collectors.toList());
		currentToken = -1;
	}

	private static List<Token> tokenize(String s) {
		LexingState state = new LexingState();
		List<Token> tokens = new ArrayList<>();

		while (state.index < s.length()) {
			try {
				tokens.add(matchToken(s, state));
			} catch (LexerException e) {
				LexerException eWithState = new LexerException(e.getMessage() + state);
				eWithState.setStackTrace(e.getStackTrace());
				throw eWithState;
			}
		}
		return tokens;
	}

	/**
	 * Returns the token that matches the longest amount of text. Updates the
	 * state with the amount of text it matched.
	 * 
	 * @param code
	 *            the whole file of code
	 * @param state
	 * @return
	 */
	private static Token matchToken(String code, LexingState state) {
		String remainingCode = code.substring(state.index);
		List<Token> potentialMatches = new ArrayList<>();

		for (final TokenType type : TokenType.values()) {
			int numMatched = type.match(remainingCode);
			if (numMatched > 0) {
				potentialMatches.add(new Token(type, remainingCode.substring(0, numMatched), state.copy()));
			}
		}

		// get token that matched the most text
		Optional<Token> matched = potentialMatches.stream()
				.max((a, b) -> Integer.compare(a.value.length(), b.value.length()));

		if (matched.isPresent()) {
			state.update(matched.get().value);
			return matched.get();
		} else {
			// If there are no potential matches
			throw new LexerException("Could not match character '" + code.charAt(state.index) + "' with a token!");
		}
	}

	/**
	 * 
	 * @return an immutable list of the lexed tokens
	 */
	public List<Token> getTokens() {
		return Collections.unmodifiableList(cleanedTokens);
	}

	public Token currentToken() {
		if (currentToken >= cleanedTokens.size()) {
			return new Token(TokenType.EOF, "<eof>");
		}

		return cleanedTokens.get(currentToken);
	}

	public Token lookahead() {
		if (currentToken + 1 >= cleanedTokens.size()) {
			return new Token(TokenType.EOF, "<eof>");
		}

		return cleanedTokens.get(currentToken + 1);
	}

	/**
	 * Eats the next token and returns it.
	 * 
	 * @return
	 */
	public Token nextToken() {
		currentToken++;
		return currentToken();
	}

	public boolean hasNext() {
		return cleanedTokens.size() - 1 > currentToken;
	}

	/**
	 * Gets a list of tokens that have already been read. Used for error message
	 * purposes.
	 * 
	 * @param n
	 *            the number of tokens to return
	 * @return
	 */
	public List<Token> getConsumedTokens(int n) {
		return getFullTokens().subList(Math.max(0, currentToken - n), currentToken + 1);
	}

	public List<Token> getFullTokens() {
		return fullTokens;
	}
}
