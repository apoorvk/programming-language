package com.proglang.lexer;

import java.util.Arrays;
import java.util.stream.Stream;

import com.proglang.ast.primitives.ValueNumeric.NumberType;
import com.proglang.lexer.NumberLexer.LexedNumberInfo;

public enum TokenType {
	WHITE_SPACE {
		@Override
		public int match(String s) {
			for (int i = 0; i < s.length(); i++) {
				if (!Character.isWhitespace(s.charAt(i))) {
					return i;
				}
			}
			return s.length();
		}
	},
	COMMENT {
		@Override
		public int match(String s) {
			if (s.startsWith("//")) {
				int i = s.indexOf("\n");
				// match to next newline or end of string
				return i > 0 ? i : s.length();
			} else {
				return 0;
			}
		}
	},
	BLOCK_COMMENT {
		@Override
		public int match(String s) {
			if (!s.startsWith("/*")) {
				return 0;
			}

			int counter = 0;
			for (int i = 0; i <= s.length() - 2; i++) {
				if (s.substring(i, i + 2).equals("/*")) {
					counter++;
					i++;
				} else if (s.substring(i, i + 2).equals("*/")) {
					counter--;
					i++;
				}
				if (counter == 0) {
					return i + 1;
				}
			}
			throw new LexerException("Unmatched block comment, expected */");
		}
	},
	/* keywords */
	VARIABLE_DEC {
		@Override
		public int match(String s) {
			return matchKeyword(s, "var", "val");
		}
	},
	FUNC_DEC {
		@Override
		public int match(String s) {
			return matchKeyword(s, "proc", "func");
		}
	},
	IF {
		@Override
		public int match(String s) {
			return matchKeyword(s, "if");
		}
	},
	ELSE {
		@Override
		public int match(String s) {
			return matchKeyword(s, "else");
		}
	},
	FOR {
		@Override
		public int match(String s) {
			return matchKeyword(s, "for");
		}
	},
	WHILE {
		@Override
		public int match(String s) {
			return matchKeyword(s, "while", "until");
		}
	},
	DO {
		@Override
		public int match(String s) {
			return matchKeyword(s, "do");
		}
	},
	BOOL {
		@Override
		public int match(String s) {
			return matchKeyword(s, "true", "false");
		}
	},
	/* symbols */
	DOT {
		@Override
		public int match(String s) {
			return matchKeyword(s, ".");
		}
	},
	OPEN_PARENS {
		@Override
		public int match(String s) {
			return matchKeyword(s, "(");
		}
	},
	CLOSE_PARENS {
		@Override
		public int match(String s) {
			return matchKeyword(s, ")");
		}
	},
	OPEN_CURLY {
		@Override
		public int match(String s) {
			return matchKeyword(s, "{");
		}
	},
	CLOSED_CURLY {
		@Override
		public int match(String s) {
			return matchKeyword(s, "}");
		}
	},
	OPERATOR {
		@Override
		public int match(String s) {
			return matchKeyword(s, "+", "-", "/", "*", "%", // arithmetic
					"&&", "||", "!", // boolean
					"<<", ">>", ">>>", "~", "^", "|", "&", // bitwise
					"..", "...", // range
					"<", "<=", "==", "!=", ">=", ">"); // comparison
		}
	},
	EQUALS {
		@Override
		public int match(String s) {
			return matchKeyword(s, "=");
		}
	},
	COLON {
		@Override
		public int match(String s) {
			return matchKeyword(s, ":");
		}
	},
	SEMI_COLON {
		@Override
		public int match(String s) {
			return matchKeyword(s, ";");
		}
	},
	COMMA {
		@Override
		public int match(String s) {
			return matchKeyword(s, ",");
		}
	},
	LAMBDA {
		@Override
		public int match(String s) {
			return matchKeyword(s, "->");
		}
	},
	STRUCT {
		@Override
		public int match(String s) {
			return matchKeyword(s, "struct");
		}
	},
	IN {
		@Override
		public int match(String s) {
			return matchKeyword(s, "in");
		}
	},
	/* literals */
	IDENTIFIER {
		@Override
		public int match(String s) {
			if (!Character.isJavaIdentifierStart(s.charAt(0))) {
				return 0;
			}

			int i = 1;
			for (; i < s.length() && Character.isJavaIdentifierPart(s.charAt(i)); i++)
				;
			return i;
		}
	},
	STRING {
		@Override
		public int match(String s) {
			if (s.startsWith("\"\"")) {
				return 2;
			}
			if (s.charAt(0) != '"') {
				return 0;
			}
			s = s.substring(1, s.length());
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '"' && (s.length() == 1 || s.charAt(i - 1) != '\\')) {
					return i + 2;
				}
			}

			throw new LexerException("Unterminated string");
		}
	},
	CHAR {
		@Override
		public int match(String s) {
			if (s.charAt(0) != '\'') {
				return 0;
			}
			if (s.charAt(1) == '\\') {
				if (s.charAt(3) != '\'') {
					if (s.substring(3).contains("\'")) {
						throw new LexerException("Char may only contain 1 char");
					} else {
						throw new LexerException("Unterminated char");
					}
				}
				return 4;
			} else {
				if (s.charAt(2) != '\'') {
					if (s.substring(2).contains("\'")) {
						throw new LexerException("Char may only contain 1 char");
					} else {
						throw new LexerException("Unterminated char");
					}
				}
				return 3;
			}
		}
	},
	NUMBER {
		// Numbers support prefixes 0x/0X (hex), 0b/0B (binary), and just 0
		// (octal)
		// They support decimals, but only in specific syntax (see
		// https://blogs.oracle.com/darcy/entry/hexadecimal_floating_point_literals)
		// They support suffixes f/F (float), d/D (double), l/L (long)
		// and also, s/S (short) and y/Y (byte) - not standard, but proposed
		// (http://mail.openjdk.java.net/pipermail/coin-dev/2009-May/001880.html)
		@Override
		public int match(String remainingCode) {
			LexedNumberInfo info = NumberLexer.match(remainingCode);
			return info != null ? info.getSize() : 0;
		}

	},
	NUMBER_CAST {
		@Override
		public int match(String s) {
			Stream<String> types = Arrays.stream(NumberType.values()).map((t) -> t.name().toLowerCase());
			return types.mapToInt((type) -> matchKeyword(s, '(' + type + ')')).max().getAsInt();
		}
	},
	EOF {
		// end of file, never match
		@Override
		public int match(String remainingCode) {
			return 0;
		}
	};

	/**
	 * A function that returns the number of characters that this token type
	 * matches from the beginning of the string.
	 * 
	 * @param remainingCode
	 * @return the number of characters matched
	 */
	public abstract int match(String remainingCode);

	private static int matchKeyword(String code, String... keywords) {
		int longestMatch = 0;

		for (String key : keywords) {
			if (code.startsWith(key) && key.length() > longestMatch) {
				longestMatch = key.length();
			}
		}
		return longestMatch;
	}
}
