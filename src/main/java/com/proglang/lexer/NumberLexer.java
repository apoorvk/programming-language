package com.proglang.lexer;

import java.util.Optional;

import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class NumberLexer {

	/**
	 * Matches a number from the given text.
	 * @param remainingCode The text to read from
	 * @return Info about the lexed number, or null if not a valid number.
	 */
	public static LexedNumberInfo match(String remainingCode) {
		int index = 0;
		NumberType type = NumberType.INT;
		StringBuilder sb = new StringBuilder();
		int radix;

		int start = remainingCode.charAt(0);
		NumberMode mode = NumberMode.DECIMAL;
		if (start == '0' && index + 1 < remainingCode.length()) {
			int prefix = remainingCode.charAt(index + 1);
			switch (prefix) {
			case 'b':
			case 'B':
				mode = NumberMode.BINARY;
				index += 2;
				break;
			case 'x':
			case 'X':
				mode = NumberMode.HEXADECIMAL;
				index += 2;
				break;
			default:
				mode = NumberMode.OCTAL;
				break;
			}
		}
		radix = mode.radix;

		boolean hasPeriod = false;
		boolean hasExponent = false;
		boolean hasNumber = false;
		boolean atStart = true;
		boolean lastCharWasUnderscore = false;
		for (; index < remainingCode.length(); index++) {
			int c = remainingCode.charAt(index);

			if (c == '.') {
				if (mode != NumberMode.HEXADECIMAL && mode != NumberMode.DECIMAL) {
					throw new LexerException("Decimal points are only allowed in decimal and hexadecimal numbers!");
				}
				if (hasPeriod) {
					if (hasNumber) {
						// We can be sure it's invalid
						throw new LexerException("Only one decimal point may be present in a number!");
					} else {
						// May be an operator
						return null;
					}
				}
				if (hasExponent) {
					throw new LexerException("Periods cannot go in the exponent portion of a hexadecimal float!");
				}
				hasPeriod = true;
				sb.appendCodePoint(c);

				// Now that we have a period, we're in either a float or a double.
				// Unless there's a suffix, we assume double.
				type = NumberType.DOUBLE;
			} else if (c == 'p' || c == 'P') {
				if (index == 0 || (index == 1 && remainingCode.charAt(0) == '.')) {
					// Not a number at all
					return null;
				}
				if (mode != NumberMode.HEXADECIMAL) {
					throw new LexerException("Hex exponentiation ('p') is only allowed in hexadecimal numbers!");
				}
				if (hasExponent) {
					throw new LexerException("Only one exponentiation may be present in a number!");
				}
				// XXX Ugly, but needed - hex exponentiation needs to be prefixed with "0x"
				// for java to understand it, while we want to strip prefixes manually in
				// all other cases.  Manually insert the "0x".
				sb.insert(0, "0x");

				// Exponentiation means it's a double or float, default double
				type = NumberType.DOUBLE;

				hasExponent = true;
				sb.appendCodePoint(c);
				if (index + 1 < remainingCode.length()) {
					if (remainingCode.charAt(index + 1) == '-') {
						sb.appendCodePoint('-');
						// One - is allowed in the exponent
						index++;
					}
					// Exponent only allows decimal numbers
					mode = NumberMode.DECIMAL;
				} else {
					throw new LexerException("Unexpected end of number after exponent!");
				}
			} else if (mode != NumberMode.HEXADECIMAL && (c == 'e' || c == 'E')) {
				if (index == 0) {
					// Not a number at all
					return null;
				}
				if (mode != NumberMode.DECIMAL) {
					throw new LexerException("Exponentiation ('e') is only allowed in decimal numbers!");
				}
				if (hasExponent) {
					throw new LexerException("Only one exponentiation may be present in a number!");
				}

				// Exponentiation means it's a double or float, default double
				type = NumberType.DOUBLE;

				sb.appendCodePoint(c);
				hasExponent = true;
				if (index + 1 < remainingCode.length()) {
					if (remainingCode.charAt(index + 1) == '-') {
						sb.appendCodePoint('-');
						// One - is allowed in the exponent
						index++;
					}
					// Exponent only allows decimal numbers
					mode = NumberMode.DECIMAL;
				} else {
					throw new LexerException("Unexpected end of number after exponent!");
				}
			} else if (c == '_') {
				if (atStart) {
					// Numbers cannot start with underscores.
					return null;
				}
				lastCharWasUnderscore = true;
			} else {
				lastCharWasUnderscore = false;

				if (!mode.isValidChar(c)) {
					if (hasNumber) {
						Optional<NumberType> suffixType = numberSuffixCharType(c);
						if (suffixType.isPresent()) {
							// Include the suffix
							index++;
							type = suffixType.get();
						}
						// Now we check to make sure there's nothing bad afterwards
						if (index < remainingCode.length()) {
							int c2 = remainingCode.charAt(index);
							if (Character.isJavaIdentifierPart(c2)) {
								throw new LexerException("Bad ending to number: '" + (char)c + "'");
							}
						}
					}
					break;
				} else {
					sb.appendCodePoint(c);
					hasNumber = true;
				}
			}
			atStart = false;
		}
		

		if (lastCharWasUnderscore) {
			throw new LexerException("Numbers cannot end with an underscore!");
		}
		if (!hasNumber) {
			// To prevent just '.'
			return null;
		}
		return new LexedNumberInfo(index, type, sb.toString(), radix);
	}
	/**
	 * Checks if the given character is a valid suffix for a number.
	 */
	private static Optional<NumberType> numberSuffixCharType(int c) {
		switch (c) {
		case 'l':
		case 'L':
			return Optional.of(NumberType.LONG);
		case 'd':
		case 'D':
			return Optional.of(NumberType.DOUBLE);
		case 'f':
		case 'F':
			return Optional.of(NumberType.FLOAT);
		// Bytes (non-standard)
		case 'y':
		case 'Y':
			return Optional.of(NumberType.BYTE);
		// Shorts (non-standard)
		case 's':
		case 'S':
			return Optional.of(NumberType.SHORT);
		default:
			return Optional.empty();
		}
	}
	/**
	 * Types of numbers: hex, decimal, binary
	 */
	static enum NumberMode {
		BINARY(2) {
			@Override
			public boolean isValidChar(int codePoint) {
				return codePoint == '0' || codePoint == '1';
			}
		},
		OCTAL(8) {
			@Override
			public boolean isValidChar(int codePoint) {
				return codePoint >= '0' && codePoint <= '7';
			}
		},
		DECIMAL(10) {
			@Override
			public boolean isValidChar(int codePoint) {
				return (codePoint >= '0' && codePoint <= '9');
			}
		},
		HEXADECIMAL(16) {
			@Override
			public boolean isValidChar(int codePoint) {
				return (codePoint >= '0' && codePoint <= '9') ||
						(codePoint >= 'A' && codePoint <= 'F') ||
						(codePoint >= 'a' && codePoint <= 'f');
			}
		};
		private NumberMode(int radix) {
			this.radix = radix;
		}
		public final int radix;
		/**
		 * Checks if the given character is valid for this type of number.
		 * 
		 * Does not need to include '_'; that is checked elsewhere.
		 */
		public abstract boolean isValidChar(int codePoint);
	}

	/**
	 * Contains information about a lexed number.
	 */
	public static class LexedNumberInfo {
		private LexedNumberInfo(int size, NumberType type, String content, int radix) {
			this.size = size;
			this.type = type;
			this.content = content;
			this.radix = radix;
		}
		private final int size;
		private final NumberType type;
		private final String content;
		private final int radix;
		/**
		 * Gets the total number of characters lexed.
		 * @return the size
		 */
		public int getSize() {
			return size;
		}
		/**
		 * Gets the type of the number.
		 * @return the type
		 */
		public NumberType getType() {
			return type;
		}
		/**
		 * Gets a filtered version of the number that should be easily parseable.
		 * Prefixes, suffixes, and underscores are removed.
		 * 
		 * @return the content
		 */
		public String getContent() {
			return content;
		}
		/**
		 * Gets the radix (base) of the number.
		 * @return the radix
		 */
		public int getRadix() {
			return radix;
		}
	}
}
