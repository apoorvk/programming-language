package com.proglang.lexer;

import java.util.Optional;

public class Token {
	public TokenType type;
	public String value;
	public Optional<LexingState> state;
	
	public Token(TokenType type, String value){
		this.type = type;
		this.value = value;
		this.state = Optional.empty();
	}
	
	public Token(TokenType type, String value, LexingState state){
		this.type = type;
		this.value = value;
		this.state = Optional.of(state);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Token [type=" + type + ", val=" + value + "]";
	}
	
}
