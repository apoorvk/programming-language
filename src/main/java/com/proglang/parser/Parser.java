package com.proglang.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.Function;
import com.proglang.ast.FunctionCallNode;
import com.proglang.ast.FunctionDefNode;
import com.proglang.ast.IfElseNode;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.SequenceExpr;
import com.proglang.ast.bool.AndNode;
import com.proglang.ast.bool.NotNode;
import com.proglang.ast.bool.OrNode;
import com.proglang.ast.comparison.NumComparisonNode;
import com.proglang.ast.comparison.NumComparisonNode.NumCompareType;
import com.proglang.ast.loops.ForEachLoopNode;
import com.proglang.ast.loops.ForLoopNode;
import com.proglang.ast.loops.WhileLoopNode;
import com.proglang.ast.ops.AdditionNode;
import com.proglang.ast.ops.BitwiseNotNode;
import com.proglang.ast.ops.BitwiseOp;
import com.proglang.ast.ops.DivisionNode;
import com.proglang.ast.ops.ModulusNode;
import com.proglang.ast.ops.MultiplicationNode;
import com.proglang.ast.ops.NumberCast;
import com.proglang.ast.ops.SubtractionNode;
import com.proglang.ast.primitives.RangeNode;
import com.proglang.ast.primitives.ValueBoolean;
import com.proglang.ast.primitives.ValueChar;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;
import com.proglang.ast.primitives.ValueString;
import com.proglang.ast.primitives.ValueVoid;
import com.proglang.ast.struct.StructAccessNode;
import com.proglang.ast.struct.StructAssignNode;
import com.proglang.ast.struct.StructCreationNode;
import com.proglang.ast.struct.StructDefNode;
import com.proglang.ast.variables.VariableAssignNode;
import com.proglang.ast.variables.VariableNode;
import com.proglang.lexer.Lexer;
import com.proglang.lexer.Token;
import com.proglang.lexer.TokenType;

public class Parser {
	private static final int NUM_TOKENS_IN_ERR_HISTORY = 5;
	private Lexer lexer;

	public Parser(Lexer lexer) {
		this.lexer = lexer;
	}

	public SequenceExpr parseProgram() {
		List<ASTExpr> statements = new ArrayList<>();

		while (lexer.hasNext()) {
			try {
				statements.add(parseExpr());
				assertNextToken(TokenType.SEMI_COLON);
				lexer.nextToken();
			} catch (ParserException e) {
				Optional<String> lastTokens = lexer.getConsumedTokens(NUM_TOKENS_IN_ERR_HISTORY).stream()
						// combine the tokens' string values
						.map((a) -> a.value).reduce((a, b) -> a + b);
				if (lastTokens.isPresent()) {
					throw new ParserException(lexer.lookahead(), "Error on token \"" + lexer.lookahead().value
							+ "\" after \"" + lastTokens.get() + "\" " + e.getMessage(), e);
				} else {
					throw new ParserException(lexer.lookahead(),
							"Error on token \"" + lexer.lookahead().value + " at beginning: " + e.getMessage());
				}
			}
		}
		return new SequenceExpr(statements, false);
	}

	public ASTExpr parseExpr() {
		switch (lexer.lookahead().type) {
		// declaring variable
		case VARIABLE_DEC:
			return variableDeclaration();
		// declaring function
		case FUNC_DEC:
			return parseFuncDef();
		// if expression
		case IF:
			return parseIf();
		// for loop
		case FOR:
			return parseForLoop();
		// while and until loops
		case WHILE:
			return parseWhileLoop();
		// do while and do until loops
		case DO:
			return parseDoLoop();
		// new scope
		case OPEN_CURLY:
			return parseCodeBlock();
		case STRUCT:
			return parseStruct();
		// expression
		default:
			return lowOp(term());
		}
	}

	private ASTExpr parseStruct() {
		assertNextToken(TokenType.STRUCT);
		lexer.nextToken();
		assertNextToken(TokenType.IDENTIFIER);
		String structName = lexer.nextToken().value;
		assertNextToken(TokenType.OPEN_CURLY);

		List<String> fields = new ArrayList<>();
		do {
			lexer.nextToken();
			assertNextToken(TokenType.IDENTIFIER);
			fields.add(lexer.nextToken().value);
		} while (lexer.lookahead().type == TokenType.COMMA);
		assertNextToken(TokenType.CLOSED_CURLY);
		lexer.nextToken();

		return new StructDefNode(structName, fields);
	}

	private ASTExpr parseFuncDef() {
		assertNextToken(TokenType.FUNC_DEC);
		lexer.nextToken();

		assertNextToken(TokenType.IDENTIFIER);
		String funcName = lexer.nextToken().value;

		// parser parameters
		assertNextToken(TokenType.OPEN_PARENS);
		lexer.nextToken();

		List<String> paramNames = new ArrayList<>();
		boolean expectParam = true;
		while (!lexer.lookahead().type.equals(TokenType.CLOSE_PARENS) && expectParam) {
			expectParam = false;
			assertNextToken(TokenType.IDENTIFIER);
			paramNames.add(lexer.nextToken().value);

			if (lexer.lookahead().type == TokenType.COMMA) {
				expectParam = true;
				lexer.nextToken();
			}
		}
		assertNextToken(TokenType.CLOSE_PARENS);
		lexer.nextToken();

		// parse block
		assertNextToken(TokenType.OPEN_CURLY);
		SequenceExpr block = parseCodeBlock();

		return new FunctionDefNode(funcName, new Function(block, paramNames));
	}

	private ASTExpr parseIf() {
		assertNextToken(TokenType.IF);
		lexer.nextToken();

		ASTExpr condition = parseExpr();

		assertNextToken(TokenType.OPEN_CURLY);
		ASTExpr block = parseCodeBlock();

		if (lexer.lookahead().type != TokenType.ELSE) {
			// just if statement
			return new IfElseNode(condition, block, new ConstantExpr(ValueVoid.VOID));
		} else {
			// else or else if
			assertNextToken(TokenType.ELSE);
			lexer.nextToken();

			if (lexer.lookahead().type == TokenType.IF) {
				// else if
				return new IfElseNode(condition, block, parseIf());
			} else {
				// else
				assertNextToken(TokenType.OPEN_CURLY);

				ASTExpr elseBlock = parseCodeBlock();

				return new IfElseNode(condition, block, elseBlock);
			}
		}
	}

	private ASTExpr parseWhileLoop() {
		assertNextToken(TokenType.WHILE);
		boolean isUntil = !lexer.nextToken().value.equals("while");

		ASTExpr condition = parseExpr();

		assertNextToken(TokenType.OPEN_CURLY);
		ASTExpr block = parseCodeBlock();

		return new WhileLoopNode(isUntil, false, condition, block);
	}

	private ASTExpr parseDoLoop() {
		assertNextToken(TokenType.DO);
		lexer.nextToken();

		assertNextToken(TokenType.OPEN_CURLY);
		ASTExpr block = parseCodeBlock();

		assertNextToken(TokenType.WHILE);
		boolean isUntil = !lexer.nextToken().value.equals("while");

		ASTExpr condition = parseExpr();

		return new WhileLoopNode(isUntil, true, condition, block);
	}

	private ASTExpr parseForLoop() {
		assertNextToken(TokenType.FOR);
		lexer.nextToken();

		assertNextToken(TokenType.OPEN_PARENS);
		lexer.nextToken();

		ASTExpr start = parseExpr();

		if (lexer.lookahead().type == TokenType.IN) {
			// foreach loop
			lexer.nextToken();
			ASTExpr iter = parseExpr();

			assertNextToken(TokenType.CLOSE_PARENS);
			lexer.nextToken();

			assertNextToken(TokenType.OPEN_CURLY);
			ASTExpr block = parseCodeBlock();

			return new ForEachLoopNode(start, iter, block);
		} else {
			// for loop
			assertNextToken(TokenType.SEMI_COLON);
			lexer.nextToken();
			ASTExpr condition = parseExpr();
			assertNextToken(TokenType.SEMI_COLON);
			lexer.nextToken();
			ASTExpr increment = parseExpr();

			assertNextToken(TokenType.CLOSE_PARENS);
			lexer.nextToken();

			assertNextToken(TokenType.OPEN_CURLY);
			ASTExpr block = parseCodeBlock();

			return new ForLoopNode(start, condition, increment, block);
		}
	}

	private SequenceExpr parseCodeBlock() {
		assertNextToken(TokenType.OPEN_CURLY);
		lexer.nextToken();

		List<ASTExpr> lines = new ArrayList<>();
		while (lexer.lookahead().type != TokenType.CLOSED_CURLY) {
			lines.add(parseExpr());
			assertNextToken(TokenType.SEMI_COLON);
			lexer.nextToken();
		}
		assertNextToken(TokenType.CLOSED_CURLY);
		lexer.nextToken();

		return new SequenceExpr(lines, true);
	}

	private ASTExpr variableDeclaration() {
		assertNextToken(TokenType.VARIABLE_DEC);
		lexer.nextToken();
		assertNextToken(TokenType.IDENTIFIER);
		String name = lexer.nextToken().value;
		assertNextToken(TokenType.EQUALS);
		lexer.nextToken();
		ASTExpr varExpr = parseExpr();
		return new VariableAssignNode(name, varExpr, true);
	}

	private ASTExpr factor() {
		return highestOp(signedExprInParens());
	}

	private ASTExpr highestOp(ASTExpr firstTerm) {
		if (lexer.lookahead().type == TokenType.OPERATOR) {
			switch (lexer.lookahead().value) {
			case "*":
				lexer.nextToken();
				return highestOp(new MultiplicationNode(firstTerm, signedExprInParens()));
			case "/":
				lexer.nextToken();
				return highestOp(new DivisionNode(firstTerm, signedExprInParens()));
			case "%":
				lexer.nextToken();
				return highestOp(new ModulusNode(firstTerm, signedExprInParens()));
			case "&&":
				lexer.nextToken();
				return highestOp(new AndNode(firstTerm, signedExprInParens()));
			case "..":
				lexer.nextToken();
				return highestOp(new RangeNode(firstTerm, signedExprInParens(), false));
			case "...":
				lexer.nextToken();
				return highestOp(new RangeNode(firstTerm, signedExprInParens(), true));
			}
		}
		return firstTerm;
	}

	private ASTExpr signedExprInParens() {
		if (lexer.lookahead().type == TokenType.OPERATOR) {
			if (lexer.lookahead().value.equals("+")) {
				lexer.nextToken();
				return exprInParens();
			} else if (lexer.lookahead().value.equals("-")) {
				lexer.nextToken();
				return new MultiplicationNode(exprInParens(),
						new ConstantExpr(new ValueNumeric((byte) -1, NumberType.NO_PROMOTE_TYPE)));
			} else if (lexer.lookahead().value.equals("!")) {
				lexer.nextToken();
				return new NotNode(exprInParens());
			} else if (lexer.lookahead().value.equals("~")) {
				lexer.nextToken();
				return new BitwiseNotNode(exprInParens());
			} else {
				throw new ParserException(lexer.lookahead(), "Unknown unary operator " + lexer.lookahead().value);
			}
		} else if (lexer.lookahead().type == TokenType.NUMBER_CAST) {
			String type = lexer.lookahead().value;
			// remove parens
			type = type.substring(1, type.length() - 1);
			lexer.nextToken();
			return new NumberCast(exprInParens(), NumberType.fromString(type));

		} else {
			return exprInParens();
		}
	}

	private ASTExpr exprInParens() {
		if (lexer.lookahead().type == TokenType.OPEN_PARENS) {
			lexer.nextToken();

			ASTExpr expr = parseExpr();

			assertNextToken(TokenType.CLOSE_PARENS);
			lexer.nextToken();

			return expr;

		} else {
			return value();
		}
	}

	private ASTExpr value() {
		Token token = lexer.nextToken();
		String value = token.value;
		TokenType val = token.type;
		switch (val) {
		case NUMBER:
			return new ConstantExpr(new ValueNumeric(value));
		case STRING:
			// remove quotes from beginning and end
			return new ConstantExpr(new ValueString(value.substring(1, value.length() - 1)));
		case BOOL:
			return new ConstantExpr(new ValueBoolean(value));
		case CHAR:
			// remove quotes from beginning and end
			return new ConstantExpr(new ValueChar(value.substring(1, value.length() - 1)));
		case IDENTIFIER:
			if (lexer.lookahead().type == TokenType.OPEN_PARENS) {
				return parseFunctionCall(value);
			} else if (lexer.lookahead().type == TokenType.OPEN_CURLY) {
				return parseStructCreation(value);
			} else if (lexer.lookahead().type == TokenType.DOT) {
				lexer.nextToken();
				assertNextToken(TokenType.IDENTIFIER);
				return new StructAccessNode(value, lexer.nextToken().value);
			} else {
				return new VariableNode(value);
			}
		default:
			throw new ParserException(token, "Unknown type" + val);
		}
	}

	private ASTExpr parseStructCreation(String name) {
		assertNextToken(TokenType.OPEN_CURLY);

		List<ASTExpr> parameters = new ArrayList<>();
		do {
			lexer.nextToken();
			parameters.add(parseExpr());
		} while (lexer.lookahead().type != TokenType.CLOSED_CURLY);
		assertNextToken(TokenType.CLOSED_CURLY);
		lexer.nextToken();

		return new StructCreationNode(name, parameters);
	}

	private ASTExpr parseFunctionCall(String functionName) {
		assertNextToken(TokenType.OPEN_PARENS);
		lexer.nextToken();

		List<ASTExpr> params = new ArrayList<>();
		boolean expectExpr = true;
		while (!lexer.lookahead().type.equals(TokenType.CLOSE_PARENS) && expectExpr) {
			expectExpr = false;
			params.add(parseExpr());

			if (lexer.lookahead().type == TokenType.COMMA) {
				expectExpr = true;
				lexer.nextToken();
			}
		}
		assertNextToken(TokenType.CLOSE_PARENS);
		lexer.nextToken();

		return new FunctionCallNode(functionName, params);
	}

	private ASTExpr term() {
		return highOp(factor());
	}

	private ASTExpr highOp(ASTExpr firstTerm) {
		if (lexer.lookahead().type == TokenType.OPERATOR) {
			switch (lexer.lookahead().value) {
			case "+":
				lexer.nextToken();
				return highOp(new AdditionNode(firstTerm, factor()));
			case "-":
				lexer.nextToken();
				return highOp(new SubtractionNode(firstTerm, factor()));
			case "||":
				lexer.nextToken();
				return highOp(new OrNode(firstTerm, factor()));
			case "^":
				lexer.nextToken();
				return highOp(new BitwiseOp("^", firstTerm, factor()));
			case "&":
				lexer.nextToken();
				return highOp(new BitwiseOp("&", firstTerm, factor()));
			}

		}
		return firstTerm;

	}

	private ASTExpr lowOp(ASTExpr firstTerm) {
		if (lexer.lookahead().type == TokenType.OPERATOR) {
			switch (lexer.lookahead().value) {
			case "<":
				lexer.nextToken();
				return lowOp(new NumComparisonNode(firstTerm, term(), NumCompareType.LESS_THAN));
			case "<=":
				lexer.nextToken();
				return lowOp(new NumComparisonNode(firstTerm, term(), NumCompareType.LESS_THAN_EQ));
			case "==":
				lexer.nextToken();
				return lowOp(new NumComparisonNode(firstTerm, term(), NumCompareType.EQ));
			case "!=":
				lexer.nextToken();
				return lowOp(new NumComparisonNode(firstTerm, term(), NumCompareType.NOT_EQ));
			case ">=":
				lexer.nextToken();
				return lowOp(new NumComparisonNode(firstTerm, term(), NumCompareType.GREATER_THAN_EQ));
			case ">":
				lexer.nextToken();
				return lowOp(new NumComparisonNode(firstTerm, term(), NumCompareType.GREATER_THAN));
			case "|":
				lexer.nextToken();
				return lowOp(new BitwiseOp("|", firstTerm, term()));
			case "<<":
				lexer.nextToken();
				return lowOp(new BitwiseOp("<<", firstTerm, term()));
			case ">>":
				lexer.nextToken();
				return lowOp(new BitwiseOp(">>", firstTerm, term()));
			case ">>>":
				lexer.nextToken();
				return lowOp(new BitwiseOp(">>>", firstTerm, term()));
			}
		} else if (lexer.lookahead().type == TokenType.EQUALS) {
			if (firstTerm instanceof VariableNode) {
				VariableNode var = (VariableNode) firstTerm;
				lexer.nextToken();
				return lowOp(new VariableAssignNode(var.getName(), term(), false));
			} else if (firstTerm instanceof StructAccessNode) {
				StructAccessNode var = (StructAccessNode) firstTerm;
				lexer.nextToken();
				return lowOp(new StructAssignNode(var.getName(), var.getFieldName(), parseExpr()));
			} else {
				throw new InterpreterException("Cannot assign value to nonvariable");
			}
		}
		return firstTerm;

	}

	private void assertNextToken(TokenType type) {
		if (lexer.lookahead().type != type) {
			throw new ParserException(lexer.lookahead(),
					"Expected token of type " + type + ", got " + lexer.lookahead().type);
		}
	}
}
