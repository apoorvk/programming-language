package com.proglang.parser;

import java.util.Optional;

import com.proglang.lexer.Token;

public class ParserException extends RuntimeException {
	private final Optional<Token> token;

	public ParserException(Token token, String msg) {
		super(msg);
		this.token = Optional.ofNullable(token);
	}
	public ParserException(Token token, String msg, Throwable e) {
		super(msg, e);
		this.token = Optional.ofNullable(token);
	}
	public ParserException(String msg) {
		this(null, msg);
	}

	public ParserException(String msg, Throwable e) {
		this(null, msg, e);
	}

	public Optional<Token> getToken() {
		return token;
	}

	@Override
	public String getMessage() {
		if (token.isPresent() && token.get().state.isPresent()) {
			return super.getMessage() + token.get().state.get();
		} else {
			return super.getMessage();
		}
	}
}
