package com.proglang.ast.ops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.UnaryOpExpr;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class BitwiseNotNode extends UnaryOpExpr {

	public BitwiseNotNode(ASTExpr expr) {
		super(expr);
	}

	@Override
	public Value simplify(Context c) {
		Value val = this.val.simplify(c);
		if (val instanceof ValueNumeric) {
			NumberType type = ((ValueNumeric) val).getType();
			switch (type) {
			case BYTE:
				return new ValueNumeric(~((ValueNumeric) val).get().byteValue(), type);
			case SHORT:
				return new ValueNumeric(~((ValueNumeric) val).get().shortValue(), type);
			case INT:
				return new ValueNumeric(~((ValueNumeric) val).get().intValue(), type);
			case LONG:
				return new ValueNumeric(~((ValueNumeric) val).get().longValue(), type);
			default:
				throw new InterpreterException("Type cannot be used with operator ~: " + type);
			}
		}

		throw new InterpreterException("Type cannot be used with operator ~: " + val.getClass().getSimpleName());
	}
}