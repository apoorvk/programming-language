package com.proglang.ast.ops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.UnaryOpExpr;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class NumberCast extends UnaryOpExpr {

	private NumberType type;

	public NumberCast(ASTExpr val, NumberType type) {
		super(val);
		this.type = type;
	}

	@Override
	public ValueNumeric simplify(Context c) {
		Value simplifiedVal = this.val.simplify(c);
		if(!(simplifiedVal instanceof ValueNumeric)){
			throw new InterpreterException("Value could not be casted to number, was " + simplifiedVal.getClass().getSimpleName());
		}
		return ((ValueNumeric) simplifiedVal).castTo(type);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumberCast other = (NumberCast) obj;
		if (type != other.type)
			return false;
		return true;
	}

}
