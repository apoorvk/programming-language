package com.proglang.ast.ops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.BinaryOpExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class DivisionNode extends BinaryOpExpr {

	public DivisionNode(ASTExpr left, ASTExpr right) {
		super(left, right);
	}

	@Override
	public ValueNumeric simplify(Context c) {
		Value left = this.left.simplify(c);
		Value right = this.right.simplify(c);

		if (!(left instanceof ValueNumeric && right instanceof ValueNumeric)) {
			throw new InterpreterException("Types cannot be divided");
		}

		ValueNumeric leftN = (ValueNumeric) left;
		ValueNumeric rightN = (ValueNumeric) right;

		NumberType type = NumberType.leastCommonType(leftN.getType(), rightN.getType());

		Number leftNum = leftN.get();
		Number rightNum = rightN.get();

		switch (type) {
		case BYTE:
			return new ValueNumeric(leftNum.byteValue() / rightNum.byteValue(), type);
		case SHORT:
			return new ValueNumeric(leftNum.shortValue() / rightNum.shortValue(), type);
		case INT:
			return new ValueNumeric(leftNum.intValue() / rightNum.intValue(), type);
		case LONG:
			return new ValueNumeric(leftNum.longValue() / rightNum.longValue(), type);
		case FLOAT:
			return new ValueNumeric(leftNum.floatValue() / rightNum.floatValue(), type);
		case DOUBLE:
			return new ValueNumeric(leftNum.doubleValue() / rightNum.doubleValue(), type);
		}

		throw new InterpreterException("Unknown type " + type);
	}

}
