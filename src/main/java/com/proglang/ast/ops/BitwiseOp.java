package com.proglang.ast.ops;

import java.util.Arrays;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.BinaryOpExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;
import com.proglang.parser.ParserException;

public class BitwiseOp extends BinaryOpExpr {

	private String op;

	public BitwiseOp(String op, ASTExpr left, ASTExpr right) {
		super(left, right);
		if(!Arrays.asList("&", "|", "^", "<<", ">>", ">>>").contains(op)){
			throw new ParserException("Unknown bitwise operator: " + op);
		}
		this.op = op;
	}

	@Override
	public Value simplify(Context c) {
		Value a = this.left.simplify(c);
		Value b = this.right.simplify(c);
		if (!(a instanceof ValueNumeric && b instanceof ValueNumeric)) {
			throw new InterpreterException("Can only use bitwise operator " + op + " on number types");
		}

		Number leftNum = ((ValueNumeric) a).get();
		Number rightNum = ((ValueNumeric) b).get();

		NumberType type = NumberType.leastCommonType(((ValueNumeric) a).getType(), ((ValueNumeric) b).getType());

		switch (type) {
		case BYTE:
			switch (op) {
			case "&":
				return new ValueNumeric(leftNum.byteValue() & rightNum.byteValue(), type);
			case "|":
				return new ValueNumeric(leftNum.byteValue() | rightNum.byteValue(), type);
			case "^":
				return new ValueNumeric(leftNum.byteValue() ^ rightNum.byteValue(), type);
			case "<<":
				return new ValueNumeric(leftNum.byteValue() << rightNum.byteValue(), type);
			case ">>":
				return new ValueNumeric(leftNum.byteValue() >> rightNum.byteValue(), type);
			case ">>>":
				return new ValueNumeric(leftNum.byteValue() >>> rightNum.byteValue(), type);
			}
		case SHORT:
			switch (op) {
			case "&":
				return new ValueNumeric(leftNum.shortValue() & rightNum.shortValue(), type);
			case "|":
				return new ValueNumeric(leftNum.shortValue() | rightNum.shortValue(), type);
			case "^":
				return new ValueNumeric(leftNum.shortValue() ^ rightNum.shortValue(), type);
			case "<<":
				return new ValueNumeric(leftNum.shortValue() << rightNum.shortValue(), type);
			case ">>":
				return new ValueNumeric(leftNum.shortValue() >> rightNum.shortValue(), type);
			case ">>>":
				return new ValueNumeric(leftNum.shortValue() >>> rightNum.shortValue(), type);
			}
		case INT:
			switch (op) {
			case "&":
				return new ValueNumeric(leftNum.intValue() & rightNum.intValue(), type);
			case "|":
				return new ValueNumeric(leftNum.intValue() | rightNum.intValue(), type);
			case "^":
				return new ValueNumeric(leftNum.intValue() ^ rightNum.intValue(), type);
			case "<<":
				return new ValueNumeric(leftNum.intValue() << rightNum.intValue(), type);
			case ">>":
				return new ValueNumeric(leftNum.intValue() >> rightNum.intValue(), type);
			case ">>>":
				return new ValueNumeric(leftNum.intValue() >>> rightNum.intValue(), type);
			}
		case LONG:
			switch (op) {
			case "&":
				return new ValueNumeric(leftNum.longValue() & rightNum.longValue(), type);
			case "|":
				return new ValueNumeric(leftNum.longValue() | rightNum.longValue(), type);
			case "^":
				return new ValueNumeric(leftNum.longValue() ^ rightNum.longValue(), type);
			case "<<":
				return new ValueNumeric(leftNum.longValue() << rightNum.longValue(), type);
			case ">>":
				return new ValueNumeric(leftNum.longValue() >> rightNum.longValue(), type);
			case ">>>":
				return new ValueNumeric(leftNum.longValue() >>> rightNum.longValue(), type);
			}
		default:
			throw new InterpreterException("Cannot use operator " + op + " on types " + type);
		}

	}

}
