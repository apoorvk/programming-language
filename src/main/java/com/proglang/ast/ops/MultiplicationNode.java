package com.proglang.ast.ops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.BinaryOpExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;
import com.proglang.ast.primitives.ValueString;

/**
 * Multiplication node of AST.
 */
public class MultiplicationNode extends BinaryOpExpr {

	public MultiplicationNode(ASTExpr left, ASTExpr right) {
		super(left, right);
	}

	@Override
	public Value simplify(Context c) {
		Value left = this.left.simplify(c);
		Value right = this.right.simplify(c);

		if (left instanceof ValueNumeric && right instanceof ValueNumeric) {
			return multiplyNumNum((ValueNumeric) left, (ValueNumeric) right);
		} else if (right instanceof ValueString && left instanceof ValueNumeric) {
			return multiplyStringNum((ValueString) right, (ValueNumeric) left);
		} else if (left instanceof ValueString && right instanceof ValueNumeric) {
			return multiplyStringNum((ValueString) left, (ValueNumeric) right);
		}

		throw new InterpreterException("Types cannot be multiplied");
	}

	/**
	 * Multiplies a string by a number. For example, "abc" * 2 = "abcabc"
	 * 
	 * @param str
	 * @param num
	 * @return
	 */
	private Value multiplyStringNum(ValueString str, ValueNumeric num) {
		String val = str.get();
		double d = (double) num.castTo(NumberType.DOUBLE).get();
		StringBuilder b = new StringBuilder();

		boolean reversed = d < 0;
		d = Math.abs(d);

		for (int i = 0; i < (int)d; i++) {
			b.append(val);
		}
		b.append(val.substring(0, (int) (val.length() * (d - (int) d))));

		if (reversed) {
			b.reverse();
		}
		return new ValueString(b.toString());
	}

	private ValueNumeric multiplyNumNum(ValueNumeric left, ValueNumeric right) {
		NumberType type = NumberType.leastCommonType(left.getType(), right.getType());

		Number leftNum = left.get();
		Number rightNum = right.get();

		switch (type) {
		case BYTE:
			return new ValueNumeric(leftNum.byteValue() * rightNum.byteValue(), type);
		case SHORT:
			return new ValueNumeric(leftNum.shortValue() * rightNum.shortValue(), type);
		case INT:
			return new ValueNumeric(leftNum.intValue() * rightNum.intValue(), type);
		case LONG:
			return new ValueNumeric(leftNum.longValue() * rightNum.longValue(), type);
		case FLOAT:
			return new ValueNumeric(leftNum.floatValue() * rightNum.floatValue(), type);
		case DOUBLE:
			return new ValueNumeric(leftNum.doubleValue() * rightNum.doubleValue(), type);
		}

		throw new InterpreterException("Unknown type " + type);
	}

}
