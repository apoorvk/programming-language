package com.proglang.ast;

import java.util.List;

public class Function {
	public final ASTExpr body;
	public final List<String> paramters;

	public Function(ASTExpr body, List<String> paramters) {
		this.body = body;
		this.paramters = paramters;
	}

}
