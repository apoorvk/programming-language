package com.proglang.ast.struct;

import java.util.List;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueVoid;

/**
 * Represents defining a struct in a program.
 *
 */
public class StructDefNode implements ASTExpr {

	private String structName;
	private List<String> fields;

	public StructDefNode(String structName, List<String> fields) {
		this.structName = structName;
		this.fields = fields;
	}

	@Override
	public Value simplify(Context c) {
		c.addStruct(structName, fields);

		return ValueVoid.VOID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		result = prime * result + ((structName == null) ? 0 : structName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StructDefNode other = (StructDefNode) obj;
		if (fields == null) {
			if (other.fields != null)
				return false;
		} else if (!fields.equals(other.fields))
			return false;
		if (structName == null) {
			if (other.structName != null)
				return false;
		} else if (!structName.equals(other.structName))
			return false;
		return true;
	}

}
