package com.proglang.ast.struct;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueStruct;

public class StructAssignNode implements ASTExpr {

	private String name;
	private String fieldName;
	private ASTExpr expr;

	public StructAssignNode(String structName, String fieldName, ASTExpr expr) {
		this.name = structName;
		this.fieldName = fieldName;
		this.expr = expr;
	}

	@Override
	public Value simplify(Context c) {
		Value var = c.getVariable(name);
		if(!(var instanceof ValueStruct)){
			throw new InterpreterException("Can only assign field on struct value, was " + var.getClass().getSimpleName());
		}
		return ((ValueStruct) var).get().put(fieldName, expr.simplify(c));
	}

	@Override
	public String toString() {
		return "StructAssignmentNode [name=" + name + ",field=" + fieldName + ",value=" + expr +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expr == null) ? 0 : expr.hashCode());
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StructAssignNode other = (StructAssignNode) obj;
		if (expr == null) {
			if (other.expr != null)
				return false;
		} else if (!expr.equals(other.expr))
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
