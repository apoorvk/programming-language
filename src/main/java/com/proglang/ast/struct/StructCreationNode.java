package com.proglang.ast.struct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueStruct;

/**
 * Represents calling the constructor of a struct in a program.
 *
 */
public class StructCreationNode implements ASTExpr {

	private String structName;
	private List<ASTExpr> parameters;

	public StructCreationNode(String funcName, List<ASTExpr> parameters) {
		this.structName = funcName;
		this.parameters = parameters;
	}

	@Override
	public Value simplify(Context c) {
		List<String> paramNames = c.getStruct(structName);
		Map<String, Value> vals = new HashMap<>();

		if (paramNames.size() != parameters.size()) {
			throw new InterpreterException("Must have " + paramNames.size() + " arguments creating struct " + structName
					+ ", was " + parameters.size());
		}
		for(int i = 0; i < paramNames.size(); i++){
			vals.put(paramNames.get(i), parameters.get(i).simplify(c));
		}
		
		return new ValueStruct(vals);
	}

	@Override
	public String toString() {
		return "StructCreationNode [structName=" + structName + ", parameters=" + parameters + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parameters == null) ? 0 : parameters.hashCode());
		result = prime * result + ((structName == null) ? 0 : structName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StructCreationNode other = (StructCreationNode) obj;
		if (parameters == null) {
			if (other.parameters != null)
				return false;
		} else if (!parameters.equals(other.parameters))
			return false;
		if (structName == null) {
			if (other.structName != null)
				return false;
		} else if (!structName.equals(other.structName))
			return false;
		return true;
	}

}
