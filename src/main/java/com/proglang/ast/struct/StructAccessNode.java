package com.proglang.ast.struct;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueStruct;

/**
 * Represents accessing a field of a struct in a program to get the value.
 *
 */
public class StructAccessNode implements ASTExpr {

	private String varName;
	private String fieldName;

	public StructAccessNode(String structName, String fieldName) {
		this.varName = structName;
		this.fieldName = fieldName;
	}

	@Override
	public Value simplify(Context c) {
		Value var = c.getVariable(varName);
		if(!(var instanceof ValueStruct)){
			throw new InterpreterException("Can only use dot operator on struct value");
		}
		return ((ValueStruct) var).get().get(fieldName);
	}

	@Override
	public String toString() {
		return "StructAccessNode [name=" + varName + ",field=" + fieldName +"]";
	}

	public String getName() {
		return varName;
	}
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((varName == null) ? 0 : varName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StructAccessNode other = (StructAccessNode) obj;
		if (varName == null) {
			if (other.varName != null)
				return false;
		} else if (!varName.equals(other.varName))
			return false;
		return true;
	}
}
