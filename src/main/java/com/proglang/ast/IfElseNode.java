package com.proglang.ast;

import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueBoolean;

public class IfElseNode implements ASTExpr {

	private ASTExpr condition;
	private ASTExpr blockTrue;
	private ASTExpr blockFalse;

	public IfElseNode(ASTExpr condition, ASTExpr blockTrue, ASTExpr blockFalse) {
		this.condition = condition;
		this.blockTrue = blockTrue;
		this.blockFalse = blockFalse;
	}

	@Override
	public Value simplify(Context c) {
		Value conditionValue = condition.simplify(c);
		if (!(conditionValue instanceof ValueBoolean)) {
			throw new InterpreterException(
					"If statement condition was not boolean, was " + conditionValue.getClass().getSimpleName());
		}
		ValueBoolean conditionBool = (ValueBoolean) conditionValue;
		if (conditionBool.get()) {
			return blockTrue.simplify(c);
		} else {
			return blockFalse.simplify(c);
		}
	}

	@Override
	public String toString() {
		return "IfElseNode [condition=" + condition + "{ " + blockTrue + "} else { " + blockFalse + "}]";
	}

}
