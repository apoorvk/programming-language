package com.proglang.ast;

import java.util.List;
import java.util.stream.Collectors;

import com.proglang.ast.primitives.Value;

/**
 * Represents calling a function in a program to get the value.
 *
 */
public class FunctionCallNode implements ASTExpr {

	private String funcName;
	private List<ASTExpr> parameters;

	public FunctionCallNode(String funcName, List<ASTExpr> parameters) {
		this.funcName = funcName;
		this.parameters = parameters;
	}

	@Override
	public Value simplify(Context c) {
		Function func = c.getFunction(funcName);

		// simplify paramters
		List<Value> v = parameters.stream().map((e) -> e.simplify(c)).collect(Collectors.toList());

		if (v.size() != func.paramters.size()) {
			throw new InterpreterException(
					"Number of parameters error, expected " + func.paramters.size() + ", got " + v.size());
		}

		// new scope for function
		c.pushScope();
		// add parameters
		for (int i = 0; i < v.size(); i++) {
			c.addVariable(func.paramters.get(i), v.get(i));
		}
		// evaluate function
		Value result = func.body.simplify(c);
		c.popScope();

		return result;
	}

}
