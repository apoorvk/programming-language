package com.proglang.ast;

import com.proglang.ast.primitives.Value;

public interface ASTExpr {
	public Value simplify(Context c);
}