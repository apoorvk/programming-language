package com.proglang.ast.comparison;

import java.util.function.Function;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.BinaryOpExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueBoolean;
import com.proglang.ast.primitives.ValueChar;
import com.proglang.ast.primitives.ValueNumeric;

/**
 * Comparison node of AST, compares two number types.
 */
public class NumComparisonNode extends BinaryOpExpr {

	private NumCompareType type;

	public NumComparisonNode(ASTExpr left, ASTExpr right, NumCompareType type) {
		super(left, right);
		this.type = type;
	}

	@Override
	public Value simplify(Context c) {
		Value left = this.left.simplify(c);
		Value right = this.right.simplify(c);

		if (left instanceof ValueNumeric && right instanceof ValueNumeric) {
			return compareNumNum((ValueNumeric) left, (ValueNumeric) right);
		} else if (left instanceof ValueChar && right instanceof ValueChar) {
			return compare(((ValueChar) left).get(), ((ValueChar) right).get());
		}

		throw new InterpreterException("Types cannot be compared with operator " + type + ": "
				+ left.getClass().getSimpleName() + ", " + right.getClass().getSimpleName());
	}

	@SuppressWarnings("unchecked")
	private ValueBoolean compareNumNum(ValueNumeric left, ValueNumeric right) {

		if (!(left.get() instanceof Comparable) || !(right.get() instanceof Comparable)) {
			throw new InterpreterException("Number types were not comparable");
		}
		int comp = ((Comparable<Number>) left.get()).compareTo(right.get());

		return new ValueBoolean(type.compare(comp));
	}

	private <T extends Comparable<T>> ValueBoolean compare(T left, T right) {
		int comp = left.compareTo(right);

		return new ValueBoolean(type.compare(comp));
	}

	@Override
	public String toString() {
		return "NumComparisonNode[type=" + type + ", " + left + ", " + right + "]";
	}

	public static enum NumCompareType {
		LESS_THAN((i) -> i < 0), LESS_THAN_EQ((i) -> i <= 0), NOT_EQ((i) -> i != 0), EQ((i) -> i == 0), GREATER_THAN_EQ(
				(i) -> i >= 0), GREATER_THAN((i) -> i > 0);

		/**
		 * Takes a number returned by a.compareTo(b) and gives whether a is less
		 * than, etc. b.
		 */
		private Function<Integer, Boolean> compareFunc;

		private NumCompareType(Function<Integer, Boolean> compareFunc) {
			this.compareFunc = compareFunc;
		}

		public boolean compare(int comp) {
			return compareFunc.apply(comp);
		}
	}

}
