package com.proglang.ast;

import java.util.ArrayList;
import java.util.List;

import com.proglang.ast.primitives.Value;

public class SequenceExpr implements ASTExpr {

	private List<ASTExpr> exprs = new ArrayList<>();
	// whether this should create a new scope for simplification
	private boolean newScope;

	public SequenceExpr(List<ASTExpr> exprs, boolean newScope) {
		this.exprs = exprs;
		this.setNewScope(newScope);
	}

	@Override
	public Value simplify(Context c) {
		if (isNewScope()) {
			c.pushScope();
		}
		for (int i = 0; i < getExpressions().size() - 1; i++) {
			getExpressions().get(i).simplify(c);
		}
		// return last expression
		Value expr = getExpressions().get(getExpressions().size() - 1).simplify(c);
		if (isNewScope()) {
			c.popScope();
		}
		return expr;
	}

	public List<ASTExpr> getExpressions() {
		return exprs;
	}

	@Override
	public String toString() {
		return "SequenceExpr [exprs=" + exprs + "]";
	}

	public boolean isNewScope() {
		return newScope;
	}

	public void setNewScope(boolean newScope) {
		this.newScope = newScope;
	}

}
