package com.proglang.ast.variables;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;

/**
 * Represents calling a variable in a program to get the value.
 *
 */
public class VariableNode implements ASTExpr {

	private String name;

	public VariableNode(String name) {
		this.name = name;
	}

	@Override
	public Value simplify(Context c) {
		return c.getVariable(getName());
	}

	@Override
	public String toString() {
		return "VariableNode [name=" + getName() + "]";
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableNode other = (VariableNode) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
