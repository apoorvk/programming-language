package com.proglang.ast.variables;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;

public class VariableAssignNode implements ASTExpr {

	private String name;
	private ASTExpr value;
	private boolean createNewVariable;

	public VariableAssignNode(String name, ASTExpr value, boolean createNewVariable) {
		this.name = name;
		this.value = value;
		this.createNewVariable = createNewVariable;
	}

	@Override
	public Value simplify(Context c) {
		Value val = value.simplify(c);
		if (createNewVariable) {
			c.addVariable(name, val);
		} else {
			c.assignVariable(name, val);
		}
		return val;
	}

	@Override
	public String toString() {
		return "VariableAssignNode [name=" + name + ", value=" + value + ",createNewValue=" + createNewVariable + "]";

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (createNewVariable ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableAssignNode other = (VariableAssignNode) obj;
		if (createNewVariable != other.createNewVariable)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
