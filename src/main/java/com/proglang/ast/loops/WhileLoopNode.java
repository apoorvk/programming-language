package com.proglang.ast.loops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueBoolean;

public class WhileLoopNode implements ASTExpr {
	private boolean isUntil;
	private boolean isDo;
	private ASTExpr condition;
	private ASTExpr block;

	/**
	 * @param isWhile True if this is an until loop, false if it's a while loop.
	 * @param isDo True if this is a do loop that always executes its contents once.
	 * @param condition The condition of the loop.
	 * @param block The block to execute.
	 */
	public WhileLoopNode(boolean isUntil, boolean isDo, ASTExpr condition, ASTExpr block) {
		this.isUntil = isUntil;
		this.isDo = isDo;
		this.condition = condition;
		this.block = block;
	}

	@Override
	public Value simplify(Context c) {
		Value result = null;
		if (isDo) {
			do {
				result = block.simplify(c);
			} while (continueLooping(c));
		} else {
			while (continueLooping(c)) {
				result = block.simplify(c);
			}
		}
		return result;
	}

	private boolean continueLooping(Context c) {
		Value conditionValue = condition.simplify(c);
		if (!(conditionValue instanceof ValueBoolean)) {
			throw new InterpreterException(
					"Loop statement condition was not boolean, was " + conditionValue.getClass().getSimpleName());
		}
		ValueBoolean conditionBool = (ValueBoolean) conditionValue;
		if (isUntil) {
			return !conditionBool.get();
		} else {
			return conditionBool.get();
		}
	}
}
