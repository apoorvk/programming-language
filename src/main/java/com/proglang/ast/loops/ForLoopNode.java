package com.proglang.ast.loops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueBoolean;

public class ForLoopNode implements ASTExpr {
	private ASTExpr start;
	private ASTExpr condition;
	private ASTExpr increment;
	private ASTExpr block;

	/**
	 * @param start Run once
	 * @param condition The condition of the loop.
	 * @param increment Run each time to process the loop context
	 * @param block The block to execute.
	 */
	public ForLoopNode(ASTExpr start, ASTExpr condition, ASTExpr increment, ASTExpr block) {
		this.start = start;
		this.condition = condition;
		this.increment = increment;
		this.block = block;
	}

	@Override
	public Value simplify(Context c) {
		Value result = null;
		this.start.simplify(c);
		while (continueLooping(c)) {
			result = this.block.simplify(c);
			this.increment.simplify(c);
		}
		return result;
	}

	private boolean continueLooping(Context c) {
		Value conditionValue = condition.simplify(c);
		if (!(conditionValue instanceof ValueBoolean)) {
			throw new InterpreterException(
					"Loop statement condition was not boolean, was " + conditionValue.getClass().getSimpleName());
		}
		ValueBoolean conditionBool = (ValueBoolean) conditionValue;
		return conditionBool.get();
	}
}
