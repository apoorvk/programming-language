package com.proglang.ast.loops;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.SequenceExpr;
import com.proglang.ast.primitives.Range;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueChar;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueRange;
import com.proglang.ast.primitives.ValueString;
import com.proglang.ast.variables.VariableAssignNode;
import com.proglang.ast.variables.VariableNode;
import com.proglang.parser.ParserException;

public class ForEachLoopNode implements ASTExpr {
	private VariableNode var;
	private ASTExpr iter;
	private SequenceExpr block;

	/**
	 * Syntax: for (var in iter) { block }
	 * @param var
	 *            The variable to assign intermediary expressions to
	 * @param iter
	 *            iterable expression, such as a list
	 * @param block
	 *            the block to run
	 */
	public ForEachLoopNode(ASTExpr var, ASTExpr iter, ASTExpr block) {
		if (!(var instanceof VariableNode)) {
			throw new ParserException("For each loop must start with variable name");
		}

		this.var = (VariableNode) var;
		this.iter = iter;
		this.block = (SequenceExpr) block;
	}

	@Override
	public Value simplify(Context c) {

		Value iterV = this.iter.simplify(c);

		if (iterV instanceof ValueRange) {
			return rangeForeach(c, (ValueRange) iterV);
		} else if (iterV instanceof ValueString) {
			return strForeach(c, (ValueString) iterV);
		} else {
			throw new InterpreterException("Cannot iterate over type " + iterV.getClass().getSimpleName());
		}
	}

	private Value rangeForeach(Context c, ValueRange iterV) {
		Range range = ((ValueRange) iterV).get();
		int left = (int) range.left.get();
		int right = (int) range.right.get();

		// we are going to manually create a new scope, so we can add our
		// variable in it
		this.block.setNewScope(false);

		Value result = null;
		if (left < right) {
			// Incrementing sequence, like 3 .. 5
			if (range.isInclusive()) {
				for (int i = left; i <= right; i++) {
					result = runBlock(c, new ValueNumeric(i));
				}
			} else {
				for (int i = left; i < right; i++) {
					result = runBlock(c, new ValueNumeric(i));
				}
			}
		} else {
			// decrementing sequence, like 5 .. 3
			if (range.isInclusive()) {
				for (int i = left; i >= right; i--) {
					result = runBlock(c, new ValueNumeric(i));
				}
			}else {
				for (int i = left; i > right; i--) {
					result = runBlock(c, new ValueNumeric(i));
				}
			}
		}

		return result;
	}

	private Value strForeach(Context c, ValueString iterV) {
		String str = iterV.get();

		// we are going to manually create a new scope, so we can add our
		// variable in it
		this.block.setNewScope(false);

		Value result = null;
		for (char ch : str.toCharArray()) {
			result = runBlock(c, new ValueChar(ch));
		}

		return result;
	}

	private Value runBlock(Context c, Value val) {
		Value result;
		// create scope, assign variable, and run block
		c.pushScope();
		new VariableAssignNode(var.getName(), new ConstantExpr(val), true).simplify(c);
		result = this.block.simplify(c);
		c.popScope();
		return result;
	}

}
