package com.proglang.ast.bool;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.UnaryOpExpr;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueBoolean;

public class NotNode extends UnaryOpExpr {

	public NotNode(ASTExpr expr) {
		super(expr);
	}

	@Override
	public Value simplify(Context c) {
		Value val = this.val.simplify(c);
		if (val instanceof ValueBoolean) {
			return new ValueBoolean(!(boolean)val.get());
		}
		
		throw new InterpreterException("Type cannot be used with operator !: " + val.getClass().getSimpleName());
	}
}