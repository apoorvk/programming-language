package com.proglang.ast.bool;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.BinaryOpExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueBoolean;

/**
 * Addition node of AST.
 */
public class OrNode extends BinaryOpExpr{

	public OrNode(ASTExpr left, ASTExpr right) {
		super(left, right);
	}

	@Override
	public Value simplify(Context c) {
		Value left = this.left.simplify(c);
		if (left instanceof ValueBoolean) {
			if (((ValueBoolean) left).get()) {
				return new ValueBoolean(true);
			} else {
				Value right = this.right.simplify(c);
				if (right instanceof ValueBoolean) {
					if (((ValueBoolean) right).get()) {
						return new ValueBoolean(true);
					} else {
						return new ValueBoolean(false);
					}
				}
			}
		}
		
		throw new InterpreterException("Types cannot be used with operator ||: " + left.getClass().getSimpleName()
				+ ", " + right.getClass().getSimpleName());
	}
}