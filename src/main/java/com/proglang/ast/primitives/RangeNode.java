package com.proglang.ast.primitives;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.BinaryOpExpr;
import com.proglang.ast.Context;
import com.proglang.ast.InterpreterException;

/**
 * Range node of AST.
 */
public class RangeNode extends BinaryOpExpr {

	private boolean inclusive;

	public RangeNode(ASTExpr left, ASTExpr right, boolean inclusive) {
		super(left, right);
		this.inclusive = inclusive;
	}

	@Override
	public Value simplify(Context c) {
		Value left = this.left.simplify(c);
		Value right = this.right.simplify(c);

		if (left instanceof ValueNumeric && right instanceof ValueNumeric) {
			return new ValueRange(new Range((ValueNumeric) left, (ValueNumeric) right, inclusive));
		}

		throw new InterpreterException("Types cannot be in a range: " + left.getClass().getSimpleName() + ", "
				+ right.getClass().getSimpleName());
	}
}