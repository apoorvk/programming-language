package com.proglang.ast.primitives;

import java.util.Map;

public class ValueStruct extends Value {
	private final Map<String, Value> values;
	
	public ValueStruct(Map<String, Value> values) {
		this.values = values;
	}
	@Override
	public Map<String, Value> get() {
		return values;
	}

	@Override
	public String toString() {
		return "ValueStruct [values=" + values.toString() + "]";
	}	
}