package com.proglang.ast.primitives;

public class Range {
	public ValueNumeric left, right;
	private boolean inclusive;

	public Range(ValueNumeric left, ValueNumeric right, boolean inclusive) {
		this.left = left;
		this.right = right;
		this.setInclusive(inclusive);
	}

	@Override
	public String toString() {
		return "Range [" + (isInclusive() ? "..." : "..") +  right + "]";
	}

	public boolean isInclusive() {
		return inclusive;
	}

	public void setInclusive(boolean inclusive) {
		this.inclusive = inclusive;
	}
	
}
