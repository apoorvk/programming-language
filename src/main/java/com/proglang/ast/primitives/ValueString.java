package com.proglang.ast.primitives;

public class ValueString extends Value {
	private final String value;

	public ValueString(String value) {		
		this.value = value;
	}

	@Override
	public String get() {
		return value;
	}

	@Override
	public String toString() {
		return "ValueString [value=" + value + "]";
	}
	
}
