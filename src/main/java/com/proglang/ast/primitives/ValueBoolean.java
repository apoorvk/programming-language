package com.proglang.ast.primitives;

public class ValueBoolean extends Value {
	private final Boolean value;
	
	public ValueBoolean(String value) {
		this.value = Boolean.valueOf(value);
	}

	public ValueBoolean(boolean b) {
		this.value = b;
	}

	@Override
	public Boolean get() {
		return value;
	}

	@Override
	public String toString() {
		return "ValueBoolean [value=" + value.toString() + "]";
	}	
}