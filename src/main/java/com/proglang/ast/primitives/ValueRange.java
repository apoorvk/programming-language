package com.proglang.ast.primitives;

public class ValueRange extends Value {
	private final Range value;

	public ValueRange(Range range) {
		this.value = range;
	}

	@Override
	public Range get() {
		return value;
	}

	@Override
	public String toString() {
		return "ValueRange [" + value.left.toString() + ", " + value.right.toString() + "]";
	}
}