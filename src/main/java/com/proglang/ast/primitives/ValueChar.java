package com.proglang.ast.primitives;

import com.proglang.parser.ParserException;

public class ValueChar extends Value {
	public ValueChar(String str) {
		if (str.length() == 1) {
			this.ch = str.charAt(0);
		} else if (str.length() != 2) {
			throw new ParserException("char too long");
		} else if (str.charAt(0) != '\\') {
			throw new ParserException("char too long");
		} else {
			this.ch = str.charAt(1);
		}
	}

	public ValueChar(char ch) {
		this.ch = ch;
	}

	private final Character ch;

	@Override
	public Character get() {
		return ch;
	}

	@Override
	public String toString() {
		return "ValueChar [ch=" + ch + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ch == null) ? 0 : ch.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ValueChar other = (ValueChar) obj;
		if (ch == null) {
			if (other.ch != null) {
				return false;
			}
		} else if (!ch.equals(other.ch)) {
			return false;
		}
		return true;
	}
}
