package com.proglang.ast.primitives;

public class ValueVoid extends Value {
	public static final ValueVoid VOID = new ValueVoid();
	
	private ValueVoid(){
		
	}

	@Override
	public Object get() {
		return null;
	}

	@Override
	public String toString() {
		return "ValueVoid";
	}	
	
}