package com.proglang.ast.primitives;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.proglang.lexer.NumberLexer;
import com.proglang.lexer.NumberLexer.LexedNumberInfo;
import com.proglang.parser.ParserException;

public class ValueNumeric extends Value {
	private final Number value;
	private final NumberType type;

	public ValueNumeric(String str) {
		LexedNumberInfo info = NumberLexer.match(str);
		this.type = info.getType();
		this.value = type.parse(info.getContent(), info.getRadix());
	}
	
	public ValueNumeric(Number value) {
		this(value, NumberType.forClass(value.getClass()));
	}
	public ValueNumeric(Number value, NumberType type) {
		if (!value.getClass().equals(type.clazz)) {
			throw new IllegalArgumentException("Value (" + value
					+ ") is not of the right type for " + type + " ("
					+ value.getClass() + "/" + type.clazz + ")");
		}
		this.value = value;
		this.type = type;
	}
	@Override
	public Number get(){
		return value;
	}
	public NumberType getType() {
		return type;
	}
	public ValueNumeric castTo(NumberType newType) {
		if (newType == this.type) {
			return this;
		}
		Number newValue = newType.cast(this.value);
		return new ValueNumeric(newValue, newType);
	}
	

	@Override
	public String toString() {
		return "ValueNumeric [value=" + value + ", type=" + type + "]";
	}
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueNumeric other = (ValueNumeric) obj;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}



	public enum NumberType {
		BYTE(Byte.class, n -> Byte.valueOf(n.byteValue()), Byte::parseByte),
		SHORT(Short.class, n -> Short.valueOf(n.shortValue()), Short::parseShort),
		INT(Integer.class, n -> Integer.valueOf(n.intValue()), Integer::parseInt),
		LONG(Long.class, n -> Long.valueOf(n.longValue()), Long::parseLong),
		FLOAT(Float.class, n -> Float.valueOf(n.floatValue()), (str, radix) -> Float.parseFloat(str)),
		DOUBLE(Double.class, n -> Double.valueOf(n.doubleValue()), (str, radix) -> Double.parseDouble(str));

		/**
		 * The base number type that does not promote other number types.
		 * 
		 * (Equivilent to {@value})
		 */
		public static final NumberType NO_PROMOTE_TYPE = BYTE;

		public final Class<? extends Number> clazz;
		private Function<Number, ? extends Number> castFunc;
		private BiFunction<String, Integer, ? extends Number> parseFunc;

		private <T extends Number> NumberType(Class<T> clazz,
				Function<Number, T> castFunc, BiFunction<String, Integer, T> parseFunc) {
			this.clazz = clazz;
			this.castFunc = castFunc;
			this.parseFunc = parseFunc;
		}

		/**
		 * Parses a number of the given type from the string.
		 * 
		 * @param str
		 * @param radix
		 * @return The parsed number
		 */
		public Number parse(String str, int radix) {
			return parseFunc.apply(str, radix);
		}
		
		public Number cast(Number number) {
			return castFunc.apply(number);
		}

		private static Map<Class<? extends Number>, NumberType> BY_CLASS = new HashMap<>();

		static {
			for (NumberType type : values()) {
				BY_CLASS.put(type.clazz, type);
			}
		}

		/**
		 * Gets the number type for the given java number class. May return null
		 * for nonstandard classes.
		 */
		public static NumberType forClass(Class<? extends Number> clazz) {
			return BY_CLASS.get(clazz);
		}

		/**
		 * Returns the lowest common type between these two types of numbers,
		 * for automatic type conversions.
		 */
		public static NumberType leastCommonType(NumberType left, NumberType right) {
			if (left.ordinal() >= right.ordinal()) {
				return left;
			} else {
				return right;
			}
		}
		public static NumberType fromString(String type) {
			switch(type){
			case "byte":
				return NumberType.BYTE;
			case "short":
				return NumberType.SHORT;
			case "int":
				return NumberType.INT;
			case "long":
				return NumberType.LONG;
			case "float":
				return NumberType.FLOAT;
			case "double":
				return NumberType.DOUBLE;
			}
			throw new ParserException("No type for: " + type);
		}
	}

	
}
