package com.proglang.ast;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueChar;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueString;
import com.proglang.ast.primitives.ValueVoid;

public class Context {

	private Deque<Map<String, Value>> variables = new ArrayDeque<>();
	private Map<String, Function> functions = new HashMap<>();
	private Map<String, List<String>> structs = new HashMap<>();
	private Scanner input;

	public Context(Scanner input) {
		this.input = input;
		pushScope();
		addStandardLib();
	}

	public void pushScope() {
		variables.push(new HashMap<>());
	}

	public void popScope() {
		variables.pop();
	}

	public Value getVariable(String s) {
		Iterator<Map<String, Value>> iterator = variables.iterator();
		while (iterator.hasNext()) {
			Map<String, Value> scope = iterator.next();
			if (scope.get(s) != null)
				return scope.get(s);
		}
		throw new InterpreterException("Variable not found: " + s);
	}

	public Function getFunction(String s) {
		Function func = functions.get(s);
		if (func == null) {
			throw new InterpreterException("Function not found: " + s);
		}
		return func;
	}

	public void addFunction(String s, Function func) {
		this.functions.put(s, func);
	}

	public void addVariable(String s, Value val) {
		variables.peek().put(s, val);
	}

	public Map<String, Value> getFlattenedVariables() {
		Map<String, Value> map = new HashMap<>();

		for (Map<String, Value> layer : variables) {
			map.putAll(layer);
		}
		return map;
	}

	public void assignVariable(String s, Value value) {
		Iterator<Map<String, Value>> iterator = variables.iterator();
		while (iterator.hasNext()) {
			Map<String, Value> scope = iterator.next();
			if (scope.get(s) != null) {
				scope.put(s, value);
				return;
			}
		}
		throw new InterpreterException("Variable not found: " + s);
	}

	private void addStandardLib() {
		addSystemFuncs();
		addOutputFuncs();
		addInputFuncs();
		addFunction("charAt", Arrays.asList("arg", "index"), (c) -> {
			ValueString arg = (ValueString) c.getVariable("arg");
			ValueNumeric index = (ValueNumeric) c.getVariable("index");
			return new ValueChar(arg.get().charAt(index.get().intValue()));
		});
		addFunction("length", Arrays.asList("arg"), (c) -> {
			ValueString arg = (ValueString) c.getVariable("arg");
			return new ValueNumeric(arg.get().length());
		});
		addFunction("toInteger", Arrays.asList("s"), (c) -> {
			ValueString arg = (ValueString) c.getVariable("s");
			return new ValueNumeric(Integer.parseInt(arg.get()));
		});
		addFunction("toChar", Arrays.asList("s"), (c) -> {
			// TODO handle more types
			ValueNumeric arg = (ValueNumeric) c.getVariable("s");
			return new ValueChar((char) arg.get().intValue());
		});
		addFunction("toDouble", Arrays.asList("s"), (c) -> {
			ValueString arg = (ValueString) c.getVariable("s");
			return new ValueNumeric(Double.parseDouble(arg.get()));
		});

	}

	private void addInputFuncs() {
		addFunction("gets", Collections.emptyList(), (c) -> {
			String str = input.nextLine();
			return new ValueString(str);
		});
	}

	private void addOutputFuncs() {
		addFunction("println", Arrays.asList("arg"), (c) -> {
			System.out.println(c.getVariable("arg").get().toString());
			return ValueVoid.VOID;
		});
		addFunction("print", Arrays.asList("arg"), (c) -> {
			System.out.print(c.getVariable("arg").get().toString());
			return ValueVoid.VOID;
		});
		addFunction("putc", Arrays.asList("arg"), (c) -> {
			Value arg = c.getVariable("arg");
			if (arg instanceof ValueNumeric) {
				System.out.print((char) (((ValueNumeric) arg).get().intValue()));
			} else if (arg instanceof ValueChar) {
				System.out.print(((ValueChar) arg).get().charValue());
			} else {
				System.err.println("Expected char or number, got " + arg);
			}
			return ValueVoid.VOID;
		});
	}

	private void addSystemFuncs() {
		addFunction("dumpv", Collections.emptyList(), (c) -> {
			System.out.println(variables);
			return ValueVoid.VOID;
		});
		addFunction("dumpf", Collections.emptyList(), (c) -> {
			System.out.println(functions);
			return ValueVoid.VOID;
		});
		addFunction("dumps", Collections.emptyList(), (c) -> {
			System.out.println(structs);
			return ValueVoid.VOID;
		});
	}

	private void addFunction(String s, List<String> parameters, java.util.function.Function<Context, Value> body) {
		Function f = new Function(new ASTExpr() {
			@Override
			public Value simplify(Context c) {
				return body.apply(c);
			}
		}, parameters);
		this.addFunction(s, f);
	}

	public void addStruct(String structName, List<String> fields) {
		this.structs.put(structName, fields);
	}

	public List<String> getStruct(String structName) {
		return structs.get(structName);
	}

}
