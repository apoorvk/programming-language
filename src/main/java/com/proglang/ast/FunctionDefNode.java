package com.proglang.ast;

import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueVoid;

/**
 * Represents defining a function in a program.
 *
 */
public class FunctionDefNode implements ASTExpr {

	private String funcName;
	private Function body;

	public FunctionDefNode(String funcName, Function func) {
		this.funcName = funcName;
		this.body = func;
	}

	@Override
	public Value simplify(Context c) {
		c.addFunction(funcName, body);

		return ValueVoid.VOID;
	}

}
