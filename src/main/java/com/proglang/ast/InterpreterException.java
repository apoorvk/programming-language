package com.proglang.ast;

public class InterpreterException extends RuntimeException {

	public InterpreterException(String msg) {
		super(msg);
	}

	public InterpreterException(Throwable e) {
		super(e);
	}

	public InterpreterException(String msg, Throwable e) {
		super(msg, e);
	}

}
