package com.proglang.parser;

import org.junit.Assert;
import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.Context;
import com.proglang.ast.bool.AndNode;
import com.proglang.ast.bool.NotNode;
import com.proglang.ast.bool.OrNode;
import com.proglang.ast.primitives.ValueBoolean;
import com.proglang.lexer.Lexer;

public class TestBooleanExpressions {

	@Test
	public void testOr() {
		assertParsesTo("true || false",
				new OrNode(
						new ConstantExpr(new ValueBoolean(true)), 
						new ConstantExpr(new ValueBoolean(false))), true);
	}
	@Test
	public void testAnd() {
		assertParsesTo("true && false",
				new AndNode(
						new ConstantExpr(new ValueBoolean(true)), 
						new ConstantExpr(new ValueBoolean(false))), false);
	}
	@Test
	public void testUnaryOps() {
		assertParsesTo("!(true || false) && !false",
				new AndNode(
						new NotNode(
								new OrNode(
										new ConstantExpr(new ValueBoolean(true)), 
										new ConstantExpr(new ValueBoolean(false)))),
						new NotNode(
								new ConstantExpr(new ValueBoolean(false)))), false);
	}
	@Test
	public void testUnaryOps2(){
		assertParsesTo("!true || false || !(true)",
				new OrNode(
						new OrNode(
								new NotNode(
										new ConstantExpr(new ValueBoolean(true))),
								new ConstantExpr(new ValueBoolean(false))), 
						new NotNode(
								new ConstantExpr(new ValueBoolean(true)))), false);
	}
	@Test
	public void testUnaryOps3() {
		assertParsesTo("! false && true || !(false && false)",
				new OrNode(
						new AndNode(
								new NotNode(
										new ConstantExpr(new ValueBoolean(false))), 
								new ConstantExpr(new ValueBoolean(true))),
						new NotNode(
								new AndNode(
										new ConstantExpr(new ValueBoolean(false)),
										new ConstantExpr(new ValueBoolean(false))))), true);
	}
	@Test
	public void testAddMultiply() {
		assertParsesTo("false || !false && !false",
				new OrNode( 
						new ConstantExpr(new ValueBoolean(false)), 
						new AndNode(
								new NotNode(new ConstantExpr(new ValueBoolean(false))), 
								new NotNode(new ConstantExpr(new ValueBoolean(false))))), true);
		assertParsesTo("true && !false || !true",
				new OrNode( 
						new AndNode(
								new ConstantExpr(new ValueBoolean(true)), 
								new NotNode(
										new ConstantExpr(new ValueBoolean(false)))),
						new NotNode(
								new ConstantExpr(new ValueBoolean(true)))), true);
	}
	@Test
	public void testOrAnd() {
		assertParsesTo("true || !false && false",
				new OrNode( 
						new ConstantExpr(new ValueBoolean(true)),
						new AndNode(
								new NotNode(
										new ConstantExpr(new ValueBoolean(false))),
								new ConstantExpr(new ValueBoolean(false)))), true);
		assertParsesTo("(false || !false) && !false",
				new AndNode(
						new OrNode(
										new ConstantExpr(new ValueBoolean(false)),
										new NotNode(
												new ConstantExpr(new ValueBoolean(false)))),
						new NotNode(
								new ConstantExpr(new ValueBoolean(false)))), true);
	}
	@Test
	public void testParseAndLeftToRight() {
		assertParsesTo("true && false && false",
				new AndNode(
						new AndNode(
										new ConstantExpr(new ValueBoolean(true)),
										new ConstantExpr(new ValueBoolean(false))),
						new ConstantExpr(new ValueBoolean(false))), false);
	}
	@Test
	public void testParseAddLeftToRight() {
		assertParsesTo("true || true || false",
				new OrNode(
						new OrNode(
										new ConstantExpr(new ValueBoolean(true)),
										new ConstantExpr(new ValueBoolean(true))),
						new ConstantExpr(new ValueBoolean(false))), true);
	}
	private final void assertParsesTo(String code, ASTExpr expectedNode, boolean result) {
		ASTExpr actualNodes = new Parser(new Lexer(code)).parseExpr();
		Assert.assertEquals(expectedNode, actualNodes);
		Assert.assertEquals(result, actualNodes.simplify(new Context(null)).get());
	}
	
}