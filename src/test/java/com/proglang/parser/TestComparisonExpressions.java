package com.proglang.parser;

import static org.junit.Assert.fail;

import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.lexer.Lexer;

public class TestComparisonExpressions {

	@Test
	public void testLessThan() {
		assertParsesTo("5 + 3 ", "<", " 9", true);
		assertParsesTo("5 * -2 ", "<", " 9 + 2", true);
	}
	@Test
	public void testGreaterThan(){
		assertParsesTo("-5 ", ">", " -7", true);
		assertParsesTo("4 ", ">", " 4", false);
	}
	@Test
	public void testGREq(){
		assertParsesTo("3 ", ">=", " 3", true);
		assertParsesTo("8 ", ">=", " 9", false);
	}
	@Test
	public void testLEQ(){
		assertParsesTo("81 / 9 ", "<=", " 64 / 8", false);
		assertParsesTo("6 + 1 * 4 ", "<= 5", " * 5 / 2", true);
	}
	@Test
	public void testEquals(){
		assertParsesTo("9 ", "==", " 8", false);
		assertParsesTo("9 ", "==", " 9", true);
		assertParsesTo("25 / 2 ", "==", " 5 * 5 / 2", true);
	}

	/**
	 * Tests that parsing {@code left + op + right} results in the expected value
	 * @param left Left side of operator
	 * @param op Equality operator to use
	 * @param right Right side of operator
	 * @param expected The expected result
	 */
	private final void assertParsesTo(String left, String op, String right, boolean expected) {
		String code = left + op + right;
		ASTExpr parsed = new Parser(new Lexer(code)).parseExpr();
		boolean value = (Boolean) parsed.simplify(new Context(null)).get();

		// More detailed info on fails
		if (value != expected) {
			ASTExpr leftParsed = new Parser(new Lexer(left)).parseExpr();
			ASTExpr rightParsed = new Parser(new Lexer(right)).parseExpr();
			fail("Expected " + code + " to eval to " + expected + ", but got " + value + "!" +
					"\nLeft: " + leftParsed.simplify(new Context(null)).get() +
					"\nRight: " + rightParsed.simplify(new Context(null)).get());
		}
	}

}