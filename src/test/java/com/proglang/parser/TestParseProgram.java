package com.proglang.parser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.proglang.lexer.Lexer;

public class TestParseProgram {

	@Test
	public void test() {
		testParses("var a = 3; var a = a + 3; var b = a;", 3);
	}

	private void testParses(String string, int numExpressions) {
		assertEquals(numExpressions, new Parser(new Lexer(string)).parseProgram().getExpressions().size());
	}

}
