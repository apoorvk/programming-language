package com.proglang.parser;

import org.junit.Assert;
import org.junit.Test;

import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.lexer.Lexer;

public class TestLoops {
	@Test
	public void testFactorial() {
		// Alternative factorial implementations
		assertResultIs("var i = 1; var result = 1; " +
				"while (i <= 4) { result = result * i; i = i + 1; };",
				new ValueNumeric(24));
		assertResultIs("var i = 1; var result = 1; " +
				"until (i > 5) { result = result * i; i = i + 1; };",
				new ValueNumeric(120));
		assertResultIs("var i = 1; var result = 1; " +
				"do { result = result * i; i = i + 1; } while (i <= 3);",
				new ValueNumeric(6));
		assertResultIs("var i = 1; var result = 1; " +
				"do { result = result * i; i = i + 1; } until (i > 6);",
				new ValueNumeric(720));
		assertResultIs("var i = 1; var result = for (j in 1 ... 6) {i = i * j;};",
				new ValueNumeric(720));
		assertResultIs("var result = 1; for (j in 3 .. 5) {result = result * j;};",
				new ValueNumeric(12));
	}
	@Test
	public void testRanges(){
		assertResultIs("var a = 3; var b = 5; var result = 1; for (j in a .. b) {result = result * j;};",
				new ValueNumeric(12));
		assertResultIs("var result = 0; for (i in 1 .. 5) { for (j in 1 .. 5) {result = result + i * j;};};",
				new ValueNumeric(100));
	}
	private final void assertResultIs(String code, Value result) {
		Context c = new Context(null);
		new Parser(new Lexer(code)).parseProgram().simplify(c);
		Assert.assertEquals(result, c.getVariable("result"));
	}
}
