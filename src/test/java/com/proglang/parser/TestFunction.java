package com.proglang.parser;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.lexer.Lexer;

public class TestFunction {

	@Test
	public void testNoParam() {
		testLastValueEquals("func a(){5;}; a();", new ValueNumeric(5));
		testLastValueEquals("func a(){4; 5;}; a();", new ValueNumeric(5));
		testLastValueEquals("func a(){5;}; func b(){4;}; a();", new ValueNumeric(5));
	}
	@Test
	public void testOneParam() {
		testLastValueEquals("func a(b){b;}; a(5);", new ValueNumeric(5));
		testLastValueEquals("func a(b){-b + 1;}; a(5);", new ValueNumeric(-4));
	}
	@Test
	public void testFibonacci() {
		testLastValueEquals("func fib(n){ if(n <= 1){n;} else {fib(n-1) + fib(n-2);}; }; fib(9);", new ValueNumeric(34));
	}
	@Test
	public void testMultipleExpr() {
		testLastValueEquals("func a(n){n + 1; n - 1; }; a(4 + 1);", new ValueNumeric(4));
	}
	@Test
	public void testTwoParam() {
		testLastValueEquals("func a(a, b){ a + b * a; }; a(5, 4);", new ValueNumeric(25));
	}

	private void testLastValueEquals(String string, Value lastExpr) {
		Context c = new Context(null);
		List<Value> expressions = new Parser(new Lexer(string)).parseProgram().getExpressions().stream()
				.map((e) -> e.simplify(c))
				.collect(Collectors.toList());
		assertEquals(lastExpr, expressions.get(expressions.size() - 1));
	}

}
