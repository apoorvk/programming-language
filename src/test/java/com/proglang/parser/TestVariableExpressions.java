package com.proglang.parser;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueString;
import com.proglang.lexer.Lexer;

public class TestVariableExpressions {

	@Test
	public void testMathExpr() {
		assertVariablesAre("var s = 5;", new HashMap<String, Value>(){{
			put("s", new ValueNumeric(5));
		}});
		assertVariablesAre("var s = 2 + 1 * 3;", new HashMap<String, Value>(){{
			put("s", new ValueNumeric(5));
		}});
	}
	@Test
	public void testString() {
		assertVariablesAre("var abc = \"abc\";", new HashMap<String, Value>(){{
			put("abc", new ValueString("abc"));
		}});
		assertVariablesAre("var variable = \"abc\" + 1;", new HashMap<String, Value>(){{
			put("variable", new ValueString("abc1"));
		}});
	}
	@Test
	public void testResettingVariables() {
		assertVariablesAre("var abc = 3; var abc = abc + 3;", new HashMap<String, Value>(){{
			put("abc", new ValueNumeric(6));
		}});
		assertVariablesAre("var abc = 3; var abc = abc * abc;", new HashMap<String, Value>(){{
			put("abc", new ValueNumeric(9));
		}});
	}

	@Test
	public void testModifyingInIf() {
		assertVariablesAre("var abc = 3; if true { abc = abc + 3; } else { };", new HashMap<String, Value>(){{
			put("abc", new ValueNumeric(6));
		}});
	}
	
	public void assertVariablesAre(String expr, Map<String, ? extends Value> expectedVars){
		Context c = new Context(null);
		
		new Parser(new Lexer(expr)).parseProgram().simplify(c);
		assertEquals(expectedVars, c.getFlattenedVariables());
	}
	

}