package com.proglang.parser;

import org.junit.Assert;
import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.Context;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueVoid;
import com.proglang.lexer.Lexer;

public class TestIf {

	@Test
	public void testCondition() {
		assertParsesTo("if true {5;} else {4;};", new ValueNumeric(5));
		assertParsesTo("if (true) {5;} else {4;};", new ValueNumeric(5));
		
		assertParsesTo("if false {5;} else {4;};", new ValueNumeric(4));
		assertParsesTo("if (false && true) {5;} else {4;};", new ValueNumeric(4));
	}
	@Test
	public void testElseIf() {
		assertParsesTo("if false {5;} else if true {4;};", new ValueNumeric(4));
		assertParsesTo("if (false) {5;};", ValueVoid.VOID);
		
		assertParsesTo("if false {5;} else if false {4;} else if true {3;} else {2;};", new ValueNumeric(3));
		assertParsesTo("if (false && true) {5;} else if false {4;};", ValueVoid.VOID);
	}
	private final void assertParsesTo(String code, Value result) {
		ASTExpr actualNodes = new Parser(new Lexer(code)).parseExpr();
		Assert.assertEquals(result, actualNodes.simplify(new Context(null)));
	}
	
}