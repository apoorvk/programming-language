package com.proglang.parser;

import org.junit.Assert;
import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.ops.AdditionNode;
import com.proglang.ast.ops.DivisionNode;
import com.proglang.ast.ops.MultiplicationNode;
import com.proglang.ast.ops.SubtractionNode;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.lexer.Lexer;

public class TestNumberExpressions {

	@Test
	public void testAdd() {
		assertParsesTo("5 + 3",
				new AdditionNode(
						new ConstantExpr(new ValueNumeric(5)), 
						new ConstantExpr(new ValueNumeric(3))));
	}
	@Test
	public void testAddNegatives() {
		assertParsesTo("-5 + -3",
				new AdditionNode(
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(5)),
								new ConstantExpr(new ValueNumeric((byte) -1))), 
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(3)),
								new ConstantExpr(new ValueNumeric((byte) -1)))));
	}
	@Test
	public void testSubtract() {
		assertParsesTo("5 - 3",
				new SubtractionNode(
						new ConstantExpr(new ValueNumeric(5)), 
						new ConstantExpr(new ValueNumeric(3))));
	}
	@Test
	public void testUnaryOps() {
		assertParsesTo("-(5 + 2) + -(3)",
				new AdditionNode(
						new MultiplicationNode(
								new AdditionNode(
										new ConstantExpr(new ValueNumeric(5)), 
										new ConstantExpr(new ValueNumeric(2))), 
								new ConstantExpr(new ValueNumeric((byte)-1))),
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(3)),
								new ConstantExpr(new ValueNumeric((byte)-1)))));
	}
	@Test
	public void testUnaryOps2(){
		assertParsesTo("- 5 + 2 + -(3)",
				new AdditionNode(
						new AdditionNode(
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(5)), 
										new ConstantExpr(new ValueNumeric((byte)-1))),
								new ConstantExpr(new ValueNumeric(2))), 
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(3)),
								new ConstantExpr(new ValueNumeric((byte)-1)))));
	}
	@Test
	public void testUnaryOps3() {
		assertParsesTo("- 5 * 3 + -(4 * 2)",
				new AdditionNode(
						new MultiplicationNode(
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(5)), 
										new ConstantExpr(new ValueNumeric((byte)-1))), 
								new ConstantExpr(new ValueNumeric(3))),
						new MultiplicationNode(
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(4)),
										new ConstantExpr(new ValueNumeric(2))),
								new ConstantExpr(new ValueNumeric((byte)-1)))));
	}
	@Test
	public void testAddMultiply() {
		assertParsesTo("5 + -1 * -3",
				new AdditionNode( 
						new ConstantExpr(new ValueNumeric(5)), 
						new MultiplicationNode(
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(1)),
										new ConstantExpr(new ValueNumeric((byte) -1))), 
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(3)),
										new ConstantExpr(new ValueNumeric((byte) -1))))));
		assertParsesTo("5 * -1 + -3",
				new AdditionNode( 
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(5)), 
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(1)),
										new ConstantExpr(new ValueNumeric((byte) -1)))),
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(3)),
								new ConstantExpr(new ValueNumeric((byte) -1)))));
	}
	@Test
	public void testAddDivide() {
		assertParsesTo("5 + -6 / -3",
				new AdditionNode( 
						new ConstantExpr(new ValueNumeric(5)), 
						new DivisionNode(
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(6)),
										new ConstantExpr(new ValueNumeric((byte) -1))), 
								new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(3)),
										new ConstantExpr(new ValueNumeric((byte) -1))))));
		assertParsesTo("(9 + -6) / -3",
				new DivisionNode(
						new AdditionNode(
										new ConstantExpr(new ValueNumeric(9)),
										new MultiplicationNode(
												new ConstantExpr(new ValueNumeric(6)),
												new ConstantExpr(new ValueNumeric((byte) -1)))),
						new MultiplicationNode(
								new ConstantExpr(new ValueNumeric(3)),
								new ConstantExpr(new ValueNumeric((byte) -1)))));
	}
	@Test
	public void testParseMultiplyLeftToRight() {
		assertParsesTo("5 * 5 / 2",
				new DivisionNode(
						new MultiplicationNode(
										new ConstantExpr(new ValueNumeric(5)),
										new ConstantExpr(new ValueNumeric(5))),
						new ConstantExpr(new ValueNumeric(2))));
	}
	@Test
	public void testParseAddLeftToRight() {
		assertParsesTo("5 + 5 - 2",
				new SubtractionNode(
						new AdditionNode(
										new ConstantExpr(new ValueNumeric(5)),
										new ConstantExpr(new ValueNumeric(5))),
						new ConstantExpr(new ValueNumeric(2))));
	}
	private final void assertParsesTo(String code, ASTExpr expectedNode) {
		ASTExpr actualNodes = new Parser(new Lexer(code)).parseExpr();
		Assert.assertEquals(expectedNode, actualNodes);
	}

}