package com.proglang.parser;

import org.junit.Assert;
import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.ops.AdditionNode;
import com.proglang.ast.ops.MultiplicationNode;
import com.proglang.ast.ops.NumberCast;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;
import com.proglang.lexer.Lexer;

public class TestCastingExpression {

	@Test
	public void testFloatCast() {
		assertParsesTo("(float) 5 + 3",
				new AdditionNode(
						new NumberCast(new ConstantExpr(new ValueNumeric(5)), NumberType.FLOAT), 
						new ConstantExpr(new ValueNumeric(3))));
	}
	@Test
	public void testIntCast() {
		assertParsesTo("(int) 5f * 3",
				new MultiplicationNode(
						new NumberCast(new ConstantExpr(new ValueNumeric(5f)), NumberType.INT), 
						new ConstantExpr(new ValueNumeric(3))));
	}
	@Test
	public void testInlineCast() {
		assertParsesTo("2 + (int) 5f * 3",
				new AdditionNode(
						new ConstantExpr(new ValueNumeric(2)),
						new MultiplicationNode(
								new NumberCast(new ConstantExpr(new ValueNumeric(5f)), NumberType.INT), 
								new ConstantExpr(new ValueNumeric(3)))));
	}
	
	private final void assertParsesTo(String code, ASTExpr expectedNode) {
		ASTExpr actualNodes = new Parser(new Lexer(code)).parseExpr();
		Assert.assertEquals(expectedNode, actualNodes);
	}

}