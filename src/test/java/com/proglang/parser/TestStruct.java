package com.proglang.parser;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueString;
import com.proglang.ast.struct.StructAccessNode;
import com.proglang.ast.struct.StructAssignNode;
import com.proglang.ast.struct.StructCreationNode;
import com.proglang.ast.struct.StructDefNode;
import com.proglang.ast.variables.VariableAssignNode;
import com.proglang.ast.variables.VariableNode;
import com.proglang.lexer.Lexer;

public class TestStruct {

	@Test
	public void testDef() {
		assertParsesTo("struct pair { a, b };", new StructDefNode("pair", Arrays.asList("a", "b")));
		assertParsesTo("struct tri { a, b, c };", new StructDefNode("tri", Arrays.asList("a", "b", "c")));
		assertParsesTo("struct one { abc };", new StructDefNode("one", Arrays.asList("abc")));
		assertParsesTo("struct address { ip, port };", new StructDefNode("address", Arrays.asList("ip", "port")));
	}
	@Test
	public void testConstruction() {
		assertParsesTo("one {1};", new StructCreationNode("one", Arrays.asList(
				new ConstantExpr(new ValueNumeric(1)))));
		assertParsesTo("pair {1, 2};", new StructCreationNode("pair", Arrays.asList(
				new ConstantExpr(new ValueNumeric(1)), 
				new ConstantExpr(new ValueNumeric(2)))));
		assertParsesTo("tri {1, 2, 3};", new StructCreationNode("tri", Arrays.asList(
				new ConstantExpr(new ValueNumeric(1)), 
				new ConstantExpr(new ValueNumeric(2)),
				new ConstantExpr(new ValueNumeric(3)))));
		assertParsesTo("address {\"192.168.1.1\", 80};",
				new StructCreationNode("address", Arrays.asList(
				new ConstantExpr(new ValueString("192.168.1.1")), 
				new ConstantExpr(new ValueNumeric(80)))));
		assertParsesTo("var a = one {a};", new VariableAssignNode("a",
				new StructCreationNode("one", Arrays.asList(
						new VariableNode("a"))),
				true));
		assertParsesTo("var b = pair {a, b};", new VariableAssignNode("b", 
				new StructCreationNode("pair", Arrays.asList(
						new VariableNode("a"), 
						new VariableNode("b"))), 
				true));
		assertParsesTo("var c = tri {a, b, c};", new VariableAssignNode("c", 
				new StructCreationNode("tri", Arrays.asList(
						new VariableNode("a"), 
						new VariableNode("b"),
						new VariableNode("c"))), 
				true));
		assertParsesTo("var ip = address {\"192.168.1.1\", 80};", new VariableAssignNode("ip",
				new StructCreationNode("address", Arrays.asList(
						new ConstantExpr(new ValueString("192.168.1.1")),
						new ConstantExpr(new ValueNumeric(80)))),
				true));
	}
	@Test
	public void testAccess() {
		assertParsesTo("a.b;", new StructAccessNode("a", "b"));
		assertParsesTo("a.b = 1;", new StructAssignNode("a", "b", new ConstantExpr(new ValueNumeric(1))));
		assertParsesTo("ip.port = 8080;", new StructAssignNode("ip", "port", new ConstantExpr(new ValueNumeric(8080))));
	}
	private final void assertParsesTo(String code, ASTExpr expectedNode) {
		ASTExpr actualNodes = new Parser(new Lexer(code)).parseExpr();
		Assert.assertEquals(expectedNode, actualNodes);
	}

}
