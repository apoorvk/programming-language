package com.proglang.ast;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.proglang.ast.primitives.ValueBoolean;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.lexer.Lexer;
import com.proglang.parser.Parser;


public class ContextTest {

	private Context context;

	@Before
	public void setUp() {
		context = new Context(null);
	}

	@Test
	public void testScope() {
		context.addVariable("a", new ValueNumeric(10));
		context.pushScope();
		context.addVariable("a", new ValueNumeric(30));
		context.pushScope();
		context.addVariable("a", new ValueNumeric(-10));

		assertEquals(-10, context.getVariable("a").get());
		context.popScope();
		assertEquals(30, context.getVariable("a").get());
	}
	@Test
	public void testScopeInCode() {
		new Parser(new Lexer("var a = 3; {var a = 5;};")).parseProgram().simplify(context);
		
		assertEquals(3, context.getVariable("a").get());
	}
	
	@Test
	public void testBoolean() {
		context.addVariable("tbool", new ValueBoolean("true"));
		context.addVariable("fbool", new ValueBoolean("false"));
		assertEquals(true, context.getVariable("tbool").get());
		assertEquals(false, context.getVariable("fbool").get());
	}
	@Test
	public void testStandardLib() {
		Assert.assertNotNull(context.getFunction("println"));
		Assert.assertNotNull(context.getFunction("print"));
	}

}
