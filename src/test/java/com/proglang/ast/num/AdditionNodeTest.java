package com.proglang.ast.num;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.proglang.ast.ConstantExpr;
import com.proglang.ast.Context;
import com.proglang.ast.ops.AdditionNode;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueChar;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class AdditionNodeTest {
	@Test
	public void testDouble() {
		testAdd(1.4, NumberType.DOUBLE, //
				2.4, NumberType.DOUBLE, //
				1.4 + 2.4, NumberType.DOUBLE);
	}

	@Test
	public void testInt() {
		testAdd(1, NumberType.INT, //
				-2, NumberType.INT, //
				1 + -2, NumberType.INT);
		ConstantExpr a = new ConstantExpr(new ValueNumeric(1));
		ConstantExpr b = new ConstantExpr(new ValueNumeric(-2));

		AdditionNode add = new AdditionNode(a, b);

		assertEquals(new ValueNumeric(-1, NumberType.INT), add.simplify(new Context(null)));
	}

	@Test
	public void testDoubleInt() {
		testAdd(1, NumberType.INT, //
				-2.4, NumberType.DOUBLE, //
				1 + -2.4, NumberType.DOUBLE);
	}

	@Test
	public void testFloatDouble() {
		testAdd(9.0f, NumberType.FLOAT, //
				-9.2, NumberType.DOUBLE, //
				9.0f + -9.2d, NumberType.DOUBLE);
	}
	@Test
	public void testChar() {
		testAdd(new ValueChar('A'), new ValueNumeric(1), new ValueNumeric((int)('A' + 1)));
	}
	private static void testAdd(Value a, Value b, Value expected) {

		AdditionNode add = new AdditionNode(new ConstantExpr(a), new ConstantExpr(b));
		Value result = add.simplify(new Context(null));

		assertEquals(expected.get(), result.get());
		assertEquals(expected, result);
	}
	
	private static void testAdd(Number a, NumberType at, Number b, NumberType bt, Number c, NumberType ct) {
		ValueNumeric an = new ValueNumeric(a);
		assertEquals(at, an.getType());
		ValueNumeric bn = new ValueNumeric(b);
		assertEquals(bt, bn.getType());

		AdditionNode add = new AdditionNode(new ConstantExpr(an), new ConstantExpr(bn));
		ValueNumeric result = (ValueNumeric) add.simplify(new Context(null));
		ValueNumeric expected = new ValueNumeric(c, ct);

		assertEquals(expected.get(), result.get());
		assertEquals(expected.getType(), result.getType());
	}
}
