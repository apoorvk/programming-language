package com.proglang.ast.num;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.proglang.ast.ConstantExpr;
import com.proglang.ast.Context;
import com.proglang.ast.ops.MultiplicationNode;
import com.proglang.ast.primitives.Value;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueString;

public class MultiplicationNodeTest {

	@Test
	public void testStringMul() {
		testMultiply(new ValueString("abc"), new ValueNumeric(2), new ValueString("abcabc"));
		testMultiply(new ValueString("abc"), new ValueNumeric(0), new ValueString(""));
		testMultiply(new ValueString("abc"), new ValueNumeric(-1), new ValueString("cba"));
		testMultiply(new ValueString("abc"), new ValueNumeric(-1.5), new ValueString("acba"));
		testMultiply(new ValueString("abc"), new ValueNumeric(1.5), new ValueString("abca"));
		testMultiply(new ValueString("abc"), new ValueNumeric(1.67), new ValueString("abcab"));
		testMultiply(new ValueString("abc"), new ValueNumeric(1.66), new ValueString("abca"));
	}

	private static void testMultiply(Value a, Value b, Value c) {

		MultiplicationNode mul = new MultiplicationNode(new ConstantExpr(a), new ConstantExpr(b));
		Value result = mul.simplify(new Context(null));

		assertEquals(c.get(), result.get());
	}
}
