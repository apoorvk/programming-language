package com.proglang.ast.num;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.proglang.ast.ASTExpr;
import com.proglang.ast.ConstantExpr;
import com.proglang.ast.Context;
import com.proglang.ast.ops.NumberCast;
import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class NumberCastTest {

	@Test
	public void testDoubleToInt() {
		ValueNumeric a = new ValueNumeric(1.4);
		ValueNumeric expected = new ValueNumeric(1, NumberType.INT);

		NumberCast cast = new NumberCast((ASTExpr) new ConstantExpr(a), NumberType.INT);
		assertCastTo(cast, expected);
	}

	@Test
	public void testIntToInt() {
		ValueNumeric a = new ValueNumeric(-1);
		ValueNumeric expected = new ValueNumeric(-1, NumberType.INT);

		NumberCast cast = new NumberCast((ASTExpr) new ConstantExpr(a), NumberType.INT);
		assertCastTo(cast, expected);
	}

	private void assertCastTo(NumberCast cast, ValueNumeric expected) {
		assertEquals(expected.getClass(), cast.simplify(new Context(null)).getClass());
		assertEquals(expected.get(), cast.simplify(new Context(null)).get());
	}

}
