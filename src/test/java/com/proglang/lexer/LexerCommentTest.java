package com.proglang.lexer;

import static com.proglang.lexer.LexerTestUtil.*;

import org.junit.Test;

public class LexerCommentTest {

	@Test
	public void testNestedComents() {
		assertLexesTo("// abc //def \n//hello",
				new Token(TokenType.COMMENT, "// abc //def "), //
				new Token(TokenType.WHITE_SPACE, "\n"), //
				new Token(TokenType.COMMENT, "//hello"));
	}
	
	@Test
	public void testBlockComments() {
		assertLexesTo("/* abc */",
				new Token(TokenType.BLOCK_COMMENT, "/* abc */"));
		assertLexesTo("/* abc \n def */",
				new Token(TokenType.BLOCK_COMMENT, "/* abc \n def */"));
		assertLexesTo("/* abc \n def */abcd/* hello */",
				new Token(TokenType.BLOCK_COMMENT, "/* abc \n def */"),
				new Token(TokenType.IDENTIFIER, "abcd"),
				new Token(TokenType.BLOCK_COMMENT, "/* hello */"));
	}
	
	@Test
	public void testNestedBlockComments(){
		assertLexesTo("/* abc /* abc */ def */",
				new Token(TokenType.BLOCK_COMMENT, "/* abc /* abc */ def */"));
		
		assertLexError("/* abc /* /* def */");
	}
	@Test
	public void testCombinedBlockComments(){
		assertLexError("/*/");
		assertLexError("ajefblejb /*/ \n aejfb");
		assertLexesTo("/**/",
				new Token(TokenType.BLOCK_COMMENT, "/**/"));
		assertLexError("/*/*/");
		assertLexError("/*//*/");
		assertLexesTo("/* // */", new Token(TokenType.BLOCK_COMMENT, "/* // */"));
	}
	@Test
	public void testCommentCombos() {
		assertLexesTo("/* abc *//* def */",
				new Token(TokenType.BLOCK_COMMENT, "/* abc */"),
				new Token(TokenType.BLOCK_COMMENT, "/* def */"));
		
		assertLexesTo("/* abc *///* def */",
				new Token(TokenType.BLOCK_COMMENT, "/* abc */"),
				new Token(TokenType.COMMENT, "//* def */"));
		
		assertLexesTo("// abc */ /* def */",
				new Token(TokenType.COMMENT, "// abc */ /* def */"));
	}

}
