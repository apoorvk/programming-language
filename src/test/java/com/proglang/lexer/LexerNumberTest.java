package com.proglang.lexer;

import static com.proglang.lexer.LexerTestUtil.assertLexError;
import static com.proglang.lexer.LexerTestUtil.assertLexesTo;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.proglang.ast.primitives.ValueNumeric;
import com.proglang.ast.primitives.ValueNumeric.NumberType;

public class LexerNumberTest {

	@Test
	public void testNumWithSpaces() {
		assertNumberLexes("1", NumberType.INT);
		assertLexesTo("1 2", new Token(TokenType.NUMBER, "1"), new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.NUMBER, "2"));
		assertLexesTo(" 2", new Token(TokenType.WHITE_SPACE, " "), new Token(TokenType.NUMBER, "2"));
		assertLexesTo("2 ", new Token(TokenType.NUMBER, "2"), new Token(TokenType.WHITE_SPACE, " "));
		assertNumberLexes("142", NumberType.INT);
		assertNumberLexes("01", NumberType.INT);
	}

	@Test
	public void testDoub() {
		assertNumberLexes("142.", NumberType.DOUBLE);
		assertNumberLexes("142.01", NumberType.DOUBLE);
		assertNumberLexes(".01", NumberType.DOUBLE);
	}


	@Test
	public void testHex() {
		assertNumberLexes("0xFF", NumberType.INT);
		assertNumberLexes("0x01234567", NumberType.INT);
		assertNumberLexes("0x79ABCDEF", NumberType.INT);
		assertLexError("0x");
		assertLexError("0xQ");
	}

	@Test
	public void testSuffixes() {
		assertNumberLexes("0l", NumberType.LONG);
		assertNumberLexes("0L", NumberType.LONG);
		assertNumberLexes("0d", NumberType.DOUBLE);
		assertNumberLexes("0D", NumberType.DOUBLE);
		assertNumberLexes("0f", NumberType.FLOAT);
		assertNumberLexes("0F", NumberType.FLOAT);
		assertNumberLexes("0s", NumberType.SHORT);
		assertNumberLexes("0S", NumberType.SHORT);
		assertNumberLexes("0y", NumberType.BYTE);
		assertNumberLexes("0Y", NumberType.BYTE);
	}

	@Test
	public void testInvalidNumbers() {
		// "Suffixes" in the middle
		assertLexError("0L99999");
		assertLexError("23y89");
		// Other stuff in the middle
		assertLexError("192q4");
		// Underscores at the end
		assertLexError("4924_");
		// Fake suffixes
		assertLexError("2924q");
	}

	@Test
	public void testOctal() {
		assertNumberLexes("0662", NumberType.INT);
		assertNumberLexes("0", NumberType.INT);
		assertLexError("09");
	}

	@Test
	public void testBinary() {
		assertNumberLexes("0b001010", NumberType.INT);
		assertLexError("0b");
		assertLexError("0b2");
	}

	@Test
	public void testHexaFloat() {
		// These things are WIERD - exponent can be + or -
		assertNumberLexes("0x0.42p20", NumberType.DOUBLE);
		assertNumberLexes("0x0.42p-20", NumberType.DOUBLE);
		assertNumberLexes("0x1p2", NumberType.DOUBLE);
		assertNumberLexes("0x1p2f", NumberType.FLOAT);
		assertNumberLexes("0x1.17Fp2F", NumberType.FLOAT);
		assertNumberLexes("0x1.17Fp2f", NumberType.FLOAT);
		assertNumberLexes("0x1.17Fp2D", NumberType.DOUBLE);
		assertNumberLexes("0x1.17Fp2d", NumberType.DOUBLE);
		assertLexError("0x1.17Fp2c"); // Exponent is in decimal
	}

	@Test
	public void testUnderscores() {
		assertNumberLexes("0b00_1010", NumberType.INT);
		assertNumberLexes("20_16", NumberType.INT);
		assertNumberLexes("020_14", NumberType.INT);
		assertNumberLexes("0x17_F", NumberType.INT);
	}

	@Test
	public void testFloatExponent() {
		assertNumberLexes("1.2e30", NumberType.DOUBLE);
		assertNumberLexes("1.2e-30", NumberType.DOUBLE);
		assertNumberLexes("1.2e30f", NumberType.FLOAT);
		assertNumberLexes("1.2e-30f", NumberType.FLOAT);
		assertNumberLexes("1e30", NumberType.DOUBLE);
		assertNumberLexes("1e30f", NumberType.FLOAT);
		assertLexError("1.2e");
	}

	private void assertNumberLexes(String numS, NumberType type) {
		assertLexesTo(numS, new Token(TokenType.NUMBER, numS));
		assertEquals(type, NumberLexer.match(numS).getType());
		
		// tries to parse this string as a number
		new ValueNumeric(numS);
		
	}
}
