package com.proglang.lexer;

import static com.proglang.lexer.LexerTestUtil.assertLexesTo;

import org.junit.Test;

public class LexerKeywordTest {

	@Test
	public void testDeclaration() {
		assertLexesTo("var abc;",
				new Token(TokenType.VARIABLE_DEC, "var"), 
				new Token(TokenType.WHITE_SPACE, " "), 
				new Token(TokenType.IDENTIFIER, "abc"), 
				new Token(TokenType.SEMI_COLON, ";"));
		assertLexesTo("val abc;",
				new Token(TokenType.VARIABLE_DEC, "val"), 
				new Token(TokenType.WHITE_SPACE, " "), 
				new Token(TokenType.IDENTIFIER, "abc"), 
				new Token(TokenType.SEMI_COLON, ";"));
		assertLexesTo("func cba() {}",
				new Token(TokenType.FUNC_DEC, "func"),
				new Token(TokenType.WHITE_SPACE, " "), 
				new Token(TokenType.IDENTIFIER, "cba"),
				new Token(TokenType.OPEN_PARENS, "("),
				new Token(TokenType.CLOSE_PARENS, ")"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPEN_CURLY, "{"),
				new Token(TokenType.CLOSED_CURLY, "}"));
	}
	
	@Test
	public void testIfElse() {
		assertLexesTo("if (true) {}",
				new Token(TokenType.IF, "if"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPEN_PARENS, "("),
				new Token(TokenType.BOOL, "true"),
				new Token(TokenType.CLOSE_PARENS, ")"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPEN_CURLY, "{"),
				new Token(TokenType.CLOSED_CURLY, "}"));
		assertLexesTo("if (false) {} else {}",
				new Token(TokenType.IF, "if"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPEN_PARENS, "("),
				new Token(TokenType.BOOL, "false"),
				new Token(TokenType.CLOSE_PARENS, ")"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPEN_CURLY, "{"),
				new Token(TokenType.CLOSED_CURLY, "}"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.ELSE, "else"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPEN_CURLY, "{"),
				new Token(TokenType.CLOSED_CURLY, "}"));
	}
}
