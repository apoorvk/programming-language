package com.proglang.lexer;

import static com.proglang.lexer.LexerTestUtil.assertLexesTo;

import org.junit.Test;

public class LexerIdentifierTest {

	@Test
	public void testSymbols() {
		assertLexesTo("$abc$", new Token(TokenType.IDENTIFIER, "$abc$"));
		assertLexesTo("var abc = cat -> cat.method();",
				new Token(TokenType.VARIABLE_DEC, "var"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.IDENTIFIER, "abc"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.EQUALS, "="),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.IDENTIFIER, "cat"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.LAMBDA, "->"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.IDENTIFIER, "cat"),
				new Token(TokenType.DOT, "."),
				new Token(TokenType.IDENTIFIER, "method"),
				new Token(TokenType.OPEN_PARENS, "("),
				new Token(TokenType.CLOSE_PARENS, ")"),
				new Token(TokenType.SEMI_COLON, ";"));
	}
	
	@Test
	public void testNumbers() {
		// assertLexError("4abc");
		assertLexesTo("abc4", new Token(TokenType.IDENTIFIER, "abc4"));
		assertLexesTo("p1", new Token(TokenType.IDENTIFIER, "p1"));
		assertLexesTo("e1", new Token(TokenType.IDENTIFIER, "e1"));
	}
	
	@Test
	public void testUnderscore() {
		assertLexesTo("_", new Token(TokenType.IDENTIFIER, "_"));
		assertLexesTo("_a_b_c_", new Token(TokenType.IDENTIFIER, "_a_b_c_"));
	}
	
	@Test
	public void testPeriod() {
		assertLexesTo("a.b", new Token(TokenType.IDENTIFIER, "a"), 
				new Token(TokenType.DOT, "."),
				new Token(TokenType.IDENTIFIER, "b"));
		assertLexesTo("a..b", new Token(TokenType.IDENTIFIER, "a"), 
				new Token(TokenType.OPERATOR, ".."),
				new Token(TokenType.IDENTIFIER, "b"));
		assertLexesTo("a...b", new Token(TokenType.IDENTIFIER, "a"), 
				new Token(TokenType.OPERATOR, "..."),
				new Token(TokenType.IDENTIFIER, "b"));
		
		assertLexesTo("...", new Token(TokenType.OPERATOR, "..."));
		assertLexesTo("..", new Token(TokenType.OPERATOR, ".."));
		assertLexesTo("(1 .. 10)", new Token(TokenType.OPEN_PARENS, "("), 
				new Token(TokenType.NUMBER, "1"),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.OPERATOR, ".."),
				new Token(TokenType.WHITE_SPACE, " "),
				new Token(TokenType.NUMBER, "10"),
				new Token(TokenType.CLOSE_PARENS, ")"));
		assertLexesTo("(1", new Token(TokenType.OPEN_PARENS, "("), 
				new Token(TokenType.NUMBER, "1"));
	}

}
