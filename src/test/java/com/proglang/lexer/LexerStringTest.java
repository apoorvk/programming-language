package com.proglang.lexer;

import static com.proglang.lexer.LexerTestUtil.assertLexesTo;
import static com.proglang.lexer.LexerTestUtil.assertLexError;

import org.junit.Test;

public class LexerStringTest {

	@Test
	public void testEmptyString() {
		assertLexesTo("\"\"", new Token(TokenType.STRING, "\"\""));
	}
	@Test
	public void testOneCharString() {
		assertLexesTo("\"a\"", new Token(TokenType.STRING, "\"a\""));
	}
	@Test
	public void testQuoteInString() {
		assertLexesTo("\"\\\"\"", new Token(TokenType.STRING, "\"\\\"\""));
	}
	@Test
	public void testNumberInString() {
		assertLexesTo("\"143\"", new Token(TokenType.STRING, "\"143\""));
	}
	@Test
	public void testUnterminatedString() {
		assertLexError("\"asf");
	}
}
