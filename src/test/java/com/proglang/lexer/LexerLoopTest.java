package com.proglang.lexer;

import static com.proglang.lexer.LexerTestUtil.*;
import static com.proglang.lexer.TokenType.*;

import org.junit.Test;

public class LexerLoopTest {
	@Test
	public void testWhile() {
		assertLexesTo("while i < 5 { i = 7; };",
				new Token(WHILE, "while"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "<"), ws(),
				new Token(NUMBER, "5"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(NUMBER, "7"), // no white space
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"), // no white space
				new Token(SEMI_COLON, ";"));

		assertLexesTo("while (i > 5) { i = i / 2; i = i + 1; };",
				new Token(WHILE, "while"), ws(),
				new Token(OPEN_PARENS, "("),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, ">"), ws(),
				new Token(NUMBER, "5"),
				new Token(CLOSE_PARENS, ")"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "/"), ws(),
				new Token(NUMBER, "2"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "+"), ws(),
				new Token(NUMBER, "1"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"),
				new Token(SEMI_COLON, ";"));
	}

	@Test
	public void testUntil() {
		assertLexesTo("until i < 5 { i = 7; };",
				new Token(WHILE, "until"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "<"), ws(),
				new Token(NUMBER, "5"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(NUMBER, "7"), // no white space
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"), // no white space
				new Token(SEMI_COLON, ";"));

		assertLexesTo("until (i <= 5) { i = i / 2; i = i + 1; };",
				new Token(WHILE, "until"), ws(),
				new Token(OPEN_PARENS, "("),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "<="), ws(),
				new Token(NUMBER, "5"),
				new Token(CLOSE_PARENS, ")"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "/"), ws(),
				new Token(NUMBER, "2"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "+"), ws(),
				new Token(NUMBER, "1"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"),
				new Token(SEMI_COLON, ";"));
	}

	@Test
	public void testDoWhile() {
		assertLexesTo("do { i = 7; } while i < 5;",
				new Token(TokenType.DO, "do"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(NUMBER, "7"), // no white space
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"), ws(),
				new Token(WHILE, "while"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "<"), ws(),
				new Token(NUMBER, "5"), // no white space
				new Token(SEMI_COLON, ";"));

		assertLexesTo("do { i = i / 2; i = i + 1; } while (i > 5);",
				new Token(TokenType.DO, "do"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "/"), ws(),
				new Token(NUMBER, "2"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "+"), ws(),
				new Token(NUMBER, "1"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"), ws(),
				new Token(WHILE, "while"), ws(),
				new Token(OPEN_PARENS, "("),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, ">"), ws(),
				new Token(NUMBER, "5"),
				new Token(CLOSE_PARENS, ")"),
				new Token(SEMI_COLON, ";"));
	}

	@Test
	public void testDoUntil() {
		assertLexesTo("do { i = 7; } until i < 5;",
				new Token(TokenType.DO, "do"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(NUMBER, "7"), // no white space
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"), ws(),
				new Token(WHILE, "until"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "<"), ws(),
				new Token(NUMBER, "5"), // no white space
				new Token(SEMI_COLON, ";"));

		assertLexesTo("do { i = i / 2; i = i + 1; } until (i <= 5);",
				new Token(TokenType.DO, "do"), ws(),
				new Token(OPEN_CURLY, "{"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "/"), ws(),
				new Token(NUMBER, "2"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(EQUALS, "="), ws(),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "+"), ws(),
				new Token(NUMBER, "1"),
				new Token(SEMI_COLON, ";"), ws(),
				new Token(CLOSED_CURLY, "}"), ws(),
				new Token(WHILE, "until"), ws(),
				new Token(OPEN_PARENS, "("),
				new Token(IDENTIFIER, "i"), ws(),
				new Token(OPERATOR, "<="), ws(),
				new Token(NUMBER, "5"),
				new Token(CLOSE_PARENS, ")"),
				new Token(SEMI_COLON, ";"));
	}
}
