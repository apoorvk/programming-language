package com.proglang.lexer;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;

public class LexerTestUtil {
	public static void assertLexesTo(String code, Token... expected){
		Lexer lexer = new Lexer(code);
		List<Token> expectedList = Arrays.asList(expected);

		Assert.assertEquals(expectedList, lexer.getFullTokens());
		//Assert.assertThat(lexer.getFullTokens(), org.hamcrest.Matchers.contains(expected));
	}
	public static void assertLexError(String code){
		try {
			Lexer lexer = new Lexer(code);
			List<Token> tokens = lexer.getTokens();
			Assert.fail("Expceted lex error, got " + tokens);
		} catch (LexerException e){
			// succeeded
		}
	}
	/**
	 * Returns a new whitespace token with a single space.
	 */
	public static Token ws() {
		return ws(" ");
	}
	/**
	 * Returns a new whitespace token with the given content
	 */
	public static Token ws(String content) {
		return new Token(TokenType.WHITE_SPACE, content);
	}
}
